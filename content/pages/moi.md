Title: <i class="fa fa-briefcase" aria-hidden="true"></i> &agrave; propos
Date: 2010-05-17 10:27
Category: <i class="fa fa-briefcase" aria-hidden="true"></i> &agrave; propos
slug: index
lang: fr
save_as: index.html
url: index.html


Je suis ingénieur d'études [CNRS](http://www.cnrs.fr) depuis 2002.

J'ai intégré le [LIMOS](https://limos.isima.fr/) en juillet 2016.

Au [LIMOS](https://limos.isima.fr/) j'interviens à différents niveaux dans des projets recherches

* montage du projet
* gestion du projet
* développement logiciel lié au projet

Au sein de l'équipe [CRI de l'ISIMA/LIMOS](https://cri.isima.fr) Je participe à la gestion

* de la plateforme d'hébergement web
* de [la forge logicielle](https://gitlab.isima.fr)

Je suis également impliqué dans la chaîne fonctionnelle [SSI](https://fr.wikipedia.org/wiki/S%C3%A9curit%C3%A9_des_syst%C3%A8mes_d%27information) du [CNRS](http://www.cnrs.fr)

* Je suis expert SSI à la [CRSSI](http://www.dr7.cnrs.fr/spip.php?rubrique856) de la délégation RHône Alpes Auvergne du CNRS

* Je suis [CSSI](http://aresu.dsi.cnrs.fr/spip.php?article120) (Correspondant Sécurité Système d'Information) pour le LIMOS

* je participe également aux actions SSI de l'UCA

Vous trouverez ici

* mon [blog](category/blog.html) qui référence des notes techniques et autres
* des supports de [cours](category/etudiants.html)
* la liste des [projets](category/projets.html) auquels je participe
* les rapports de [recherche](category/recherche.html) aux quels j'ai participé
