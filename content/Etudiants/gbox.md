Title: G-Box
Date: 2017-09-20 10:15
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, sécurité

## Contexte

La G-box est un routeur avec des fonctionnalités de sécurité, elle permet notamment de filtrer les flux sortant à des fins de contrôle parental ou de filtre anti-phishing.

Tout le trafic entre les ordinateurs du réseau local et internet doit passer par la G-box.

la G-box a pour de but valider ou non le trafic.

Elle utilise pour se faire 3 stratégies

* les DNS ([OpenDNS Family Shield](https://www.opendns.com/home-internet-security/) pour le contrôle parentale par exemple)
* l'inspection d'url
* l'inspection de contenu

La G-Box est un routeur domestique qui doit nécessiter le moins de configuration possible. Elle ne nécessite notamment aucune intervention sur les postes clients

La configuration du filtrage doit pouvoir se faire de manière sécurisée au travers d'une interface web.

Idéalement c'est un boîtier qui présente donc au moins 2 interfaces:

* l'interface publique, que vous connecterez sur la box du FAI
* la ou les interfaces privées, sur lesquelles viennent se connecter les appareils domestiques (PC fixe, laptop, tablette, téléhpone)

Pour le prototype de la G-Box le [raspberry pi 3b](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/) est pressentie.
L'objectif de ce projet est d'arriver à une PoC (Proof Of Concept) permettant de valider l'implémentation des fonctions de filtrage.

Un ou deux raspberry vous seront fournis pour vos tests.

## Fonctionnalités à implémenter

* gestion d'une liste de sites autorisés / interdits par périphériques
* gestion de la liste de mots clés autorisés / interdits par périphériques
* gestion du DNS par périphériques
* système de notification lorsqu'une tentative d'outre passer une règle est détectée

## Résultat attendu

un dépôt sur [https://gitlab.isima.fr](https://gitlab.isima.fr) contenant

* une image de système déployable sur un raspberry pi
* un script de provisioning permettant le déploiement des fonctionnalités implémentées
* un fichier `README.md` documentant l'installation, la configuration et l'utilisation du service
* un rapport détaillé du travail réalisé

## Technologies

* [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [SSL](https://fr.wikipedia.org/wiki/SSL), [Raspberry Pi](https://www.raspberrypi.org/),  [Raspbian](https://raspbian-france.fr/), [Réseau informatique](https://fr.wikipedia.org/wiki/R%C3%A9seau_informatique)

## Points à considérer

* la sécurité globale du système
    * accès physique
    * MAC spoofing
    * configuraiton manuelle des DNS sur les postes clients
    * gestion des connection SSL en mode filtrage par urls ou par mots clés
