Title: ZZ2 F5 - Securité logicielle (2/2) - sécurité des applications web
Date: 2019-11-20 10:55
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: cours

[TOC]

## Plan du cours

### Architecture

* [HTTP](slides/1337/http.html)
* [HTTPS](slides/privacy/tls.html#/0/52)
* [JS](slides/1337/js.html)

### Pentesting

* [Collecter](slides/1337/collecting.html)
* [Détecter](slides/1337/detecting.html)
    * [Heartbleed](slides/1337/heartbleed.html)

### Mécanisme

* [Authentification](slides/1337/authentication.html)

### Vulnérabilités communes

* [Command execution](slides/1337/cmdi.html)
    * [Shellshock](slides/1337/shellshock.htm)
* [Upload](slides/1337/upload.htm)
* [LFI_RFI](slides/1337/fi.htm)
* [XSS](slides/1337/xss.html)
* [CSRF](slides/1337/csrf.html)
* [SQLi](slides/1337/sqli.htm)
    * [Drupalgeddon](slides/1337/drupalgeddon.htm!)

### Se protéger

* [Top10](slides/1337/top10.htm)
* [anticiper](slides/1337/anticiper.htm)

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">FYI</h3>
  </div>
  <div class="panel-body">
    <ul>
      <li>Tous les slides sont fait avec <a href="https://github.com/hakimel/reveal.js">reveal.js</a>
        <ul>
          <li>ils sont exportables en pdf en ajoutant <code>?print-pdf#</code> à l'url (à coller juste après le <code>.html</code>) et en passant par l'impression dans un fichier du navigateur chrome ou (mieux) <a href="https://www.chromium.org/">chromium</a>
            <ul>
              <li>plus de détails sur l'<a href="https://github.com/hakimel/reveal.js/#pdf-export">export PDF de reveal</a></li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>

## Recréer l'environnement de cours dans VirtualBox

* testé avec [VirtualBox 5.2.18](https://download.virtualbox.org/virtualbox/5.2.18/virtualbox-5.2_5.2.18-124319~Ubuntu~bionic_amd64.deb) sous [Ubuntu Bionic](http://releases.ubuntu.com/bionic/)
  * et les [extensions pack associés](https://download.virtualbox.org/virtualbox/5.2.18/Oracle_VM_VirtualBox_Extension_Pack-5.2.18.vbox-extpack)

```
VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-5.2.18.vbox-extpack
```

sous windows vous devrez peut être utiliser le path entier de vboxmanage

```
"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe"
```

### Créer un réseau NAT

```bash
vboxmanage  natnetwork add --netname natwebsec --network "172.16.76.0/24" --enable --dhcp off
```

### Télécharger les images OVA

voir [https://drive.mesocentre.uca.fr/d/69e5535b0b88425396d7/](https://drive.mesocentre.uca.fr/d/69e5535b0b88425396d7/)

```bash
wget https://drive.mesocentre.uca.fr/d/69e5535b0b88425396d7/files/?p=/debian.ova&dl=1
wget https://drive.mesocentre.uca.fr/d/69e5535b0b88425396d7/files/?p=/proxy.ova&dl=1
wget https://drive.mesocentre.uca.fr/d/69e5535b0b88425396d7/files/?p=/kali.ova&dl=1
wget https://drive.mesocentre.uca.fr/d/69e5535b0b88425396d7/files/?p=/thenetwork.ova&dl=1
wget https://drive.mesocentre.uca.fr/d/69e5535b0b88425396d7/files/?p=/ubuntu-server-18.04.ova&dl=1
```
<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">FYI</h3>
  </div>
  <div class="panel-body">
    il y a environ 7 Go d'images, n'hésitez pas à vous les faire passer via des clés USB
  </div>
</div>

### Importer les images OVA

```bash
vboxmanage import debian.ova
vboxmanage import proxy.ova
vboxmanage import kali.ova
vboxmanage import thenetwork.ova
vboxmanage ubuntu-server-18.04.ova
```

### Configurer le réseau pour chaque vm

```bash
vboxmanage modifyvm debian --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm proxy --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm kali --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm thenetwork --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm ubuntu-server-18.04 --nic1 natnetwork --nat-network1 natwebsec
```

![réseau vm](images/etudiants/vm-network.png)

### (optionnel) Mettre en place le port-forwarding sur debian

```bash
vboxmanage natnetwork modify --netname natwebsec --port-forward-4 "ssh:tcp:[127.0.0.1]:1722:[172.16.76.142]:22"
vboxmanage natnetwork modify --netname natwebsec --port-forward-4 "ssh:tcp:[127.0.0.1]:1723:[172.16.76.143]:22"
vboxmanage natnetwork modify --netname natwebsec --port-forward-4 "ssh:tcp:[127.0.0.1]:1724:[172.16.76.144]:22"
vboxmanage natnetwork modify --netname natwebsec --port-forward-4 "ssh:tcp:[127.0.0.1]:1725:[172.16.76.145]:22"
vboxmanage natnetwork modify --netname natwebsec --port-forward-4 "ssh:tcp:[127.0.0.1]:1726:[172.16.76.146]:22"
```

### (optionnel) Se connecter en ssh

```bash
ssh -p 1722 mazenovi@127.0.0.1 #thenetwork
ssh -p 1723 mazenovi@127.0.0.1 #proxy
ssh -p 1724 mazenovi@127.0.0.1 #debian
ssh -p 1725 mazenovi@127.0.0.1 #kali
ssh -p 1726 mazenovi@127.0.0.1 #ubuntu server 18.04
```

## (fix) En cas de réseau injoignable sur proxy et thenetwork

si

```bash
ping 172.16.76.145 # ping sur kali
```

renvoie

```bash
connect: Network is unreachable
```

vérifier le numéro de votre interface réseau

```bash
student@proxy:~$ ifconfig -a

eth2      Link encap:Ethernet  HWaddr 08:00:27:ae:b5:20
          inet adr:172.16.76.143  Bcast:172.16.76.255  Masque:255.255.255.0
          adr inet6: fe80::a00:27ff:feae:b520/64 Scope:Lien
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          Packets reçus:24 erreurs:0 :0 overruns:0 frame:0
          TX packets:32 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 lg file transmission:1000
          Octets reçus:4789 (4.7 KB) Octets transmis:4679 (4.6 KB)

lo        Link encap:Boucle locale
          inet adr:127.0.0.1  Masque:255.0.0.0
          adr inet6: ::1/128 Scope:Hôte
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          Packets reçus:54 erreurs:0 :0 overruns:0 frame:0
          TX packets:54 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 lg file transmission:0
          Octets reçus:4076 (4.0 KB) Octets transmis:4076 (4.0 KB)
```

par exemple ce numéro peut être eth2 (comme ci dessus) au lieu de eth0

il faut alors modifier le fichier /etc/network/interfaces en fonction

```bash
student@proxy:~$ sudo vi /etc/network/interfaces

# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth2
iface eth2 inet static
        address 172.16.76.143
        netmask 255.255.255.0
        gateway 172.16.76.1
```

puis activer l'interface réseau

```bash
student@proxy:~$ sudo ifup eth2
```

réessayer

```bash
ping 172.16.76.145 # ping sur kali
```

Ce bug est dû à la numérotation fantaisiste d'Ubuntu des interfaces réseau ...


## liste des vms / noms de domaine

```
# SecLab
172.16.76.143 proxy secured heart.bleed fo.ol #proxied version of dum.my

172.16.76.144 good.one go.od targ.et
172.16.76.144 mutillid.ae
172.16.76.144 dvwa.com dv.wa
172.16.76.144 d.oc
172.16.76.144 dum.my
172.16.76.144 drup.al hackable-drupal.com drupal
172.16.76.144 wordpre.ss bl.og wp wordpress
172.16.76.144 spip sp.ip
172.16.76.145 bad.guy hack.er 1337.net

172.16.76.142 thenetwork

172.16.76.1   us.er
```

## Evaluation

* Examen écrit en fin de session

<!--
## Mini projet en binôme

* [Enoncé](https://drive.mesocentre.uca.fr/f/d9e76a8e45934a069890/?dl=1)

* [Enoncé](https://drive.mesocentre.uca.fr/f/54bdd1a80c184bbcb63e/?dl=1)

* Rendu le 25/03/2019 à 23h59 dernier délais

    * à [vincent.mazenod@uca.fr](mailto:vincent.mazenod@uca.fr)

      * ```[TP websec]``` dans le sujet du mail ... sinon je vous perds ;)

    * Tous les fichiers nommés en NOMETUDIANT1_NOMETUDIANT2_nomfichier.ext

 -->
 
## Evaluation du cours

Vous avez aimé ou vous avez détesté ce cours ... [donnez moi votre avis et aidez moi à l'améliorer (en tout anonymat)](https://docs.google.com/forms/d/1w65KH2cnL_DbTKrUT-2AMvQ_p0Ht-wfSJT2YLEB8l7E/prefill)


## See also

* [faire son propre seclab](https://blog.mazenod.fr/faire-son-propre-seclab.html)
