Title: ZZ2 F5 - Securité logicielle (1/2) - Privacy & Crypto
Date: 2019-11-16 10:55
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: cours

[TOC]

## Plan du cours

### Privacy

* [je n'ai rien à cacher](slides/privacy/jnarac.html)
* [mots de passes](slides/privacy/passwords.html)
* [tracking](slides/privacy/tracking.html)
* [souveraineté](slides/privacy/sovereignty.html)

### Crypto

* [principes cryptographiques](slides/privacy/crypto.html)
* [tls](slides/privacy/tls.html)

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">FYI</h3>
  </div>
  <div class="panel-body">
    <ul>
      <li>Tous les slides sont fait avec <a href="https://github.com/hakimel/reveal.js">reveal.js</a>
        <ul>
          <li>ils sont exportables en pdf en ajoutant <code>?print-pdf#</code> à l'url (à coller juste après le <code>.html</code>) et en passant par l'impression dans un fichier du navigateur chrome ou (mieux) <a href="https://www.chromium.org/">chromium</a>
            <ul>
              <li>plus de détails sur l'<a href="https://github.com/hakimel/reveal.js/#pdf-export">export PDF de reveal</a></li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>

## Evaluation

* Examen écrit en fin de session

## Environnement de TP

* N'importe quelle VMs fresh de type debian fonctionnera avec les commandes du support de cours

<!--
### déployer et configurer TLS  avec Apache

* [https://gitlab.isima.fr/vimazeno/tp-www-ssl](https://gitlab.isima.fr/vimazeno/tp-www-ssl)

## Mini projet en binôme

* Donnez une manière de générer un [QR-code permettant d'importer directement une clé publique PGP avec OpenKeyChain](https://github.com/open-keychain/open-keychain/wiki/QR-Codes)
  * listez les outils utilisés
  * détaillez les commandes à effectuer pour passer de la clé publique au QR-Code
  * détaillez comment vous avecz tester le contenu de votre QR-Code
  * joignez le QR-Code de votre clé PGP au message
* Publiez votre clé PGP sur https://pgp.mit.edu puis envoyez moi l'url de votre clé
* Signez ma clé PGP isima https://fc.isima.fr/~mazenod/pages/pgp.html ou https://pgp.mit.edu/pks/lookup?op=vindex&search=0x408A4E510DCD0D14 et envoyez là moi en pièce jointe de votre message (2 points)
* Votre message devra être chiffré, signé et comporter votre clé publique en pièce jointe

* Rendu le 25/03/2019 à 23h59 dernier délais

  * à [vincent.mazenod@uca.fr](mailto:vincent.mazenod@uca.fr)

    * ```[TP privacy crypto]``` dans le sujet du mail ... sinon je vous perds ;)


    * Tous les fichiers nommés en NOMETUDIANT1_NOMETUDIANT2_nomfichier.ext

-->

## Evaluation du cours

Vous avez aimé ou vous avez détesté ce cours ... [donnez moi votre avis et aidez moi à l'améliorer (en tout anonymat)](https://docs.google.com/forms/d/1w65KH2cnL_DbTKrUT-2AMvQ_p0Ht-wfSJT2YLEB8l7E/prefill)
