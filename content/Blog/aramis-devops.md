Title: Bonnes pratiques en développement web (Frameworks, CLI, tests, intégration continue, git, github gitlab)
Date: 2017-04-13 10:55
Category: <i class='fa fa-bullhorn' aria-hidden='true'></i> Blog

Dans le cadre de la [9<sup>ème</sup> journée annuelle d'Aramis](http://aramis.resinfo.org/wiki/doku.php?id=pleniaires:pleniere13avril2017) le 13 avril 2017 à Saint-Etienne sur le Campus Tréfilerie à la Maison de l'Université Jean-Monnet.

Le thème était Dev & Ops : les outils du dialogue.

<iframe width="800" height="600" src="https://doc.m4z3.me/_/aramis/git.htm">
</iframe>

[support PDF](http://aramis.resinfo.org/wiki/lib/exe/fetch.php?media=pleniaires:vm-gitlab-dev_3ops.pdf), [support en ligne](http://doc.m4z3.me/_/aramis/git.htm), [vidéo](https://webcast.in2p3.fr/video/bonnes_pratiques_en_developpement_web)
