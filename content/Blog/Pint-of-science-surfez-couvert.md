Title: Pint of Science - Surfez couvert
Date: 2018-05-15 19:00
Category: <i class='fa fa-bullhorn' aria-hidden='true'></i> Blog

Dans le cadre de <a href="https://pintofscience.fr">Pint of Science</a>, je suis intervenu lors d'une session <a href="https://pintofscience.fr/event/big-brother">Big Brother</a> pour expliquer les enjeux de vie privée et de protection des sources sur le web.

lien vers les slides: <a href="http://m4z3.me/pintofscience">http://m4z3.me/pintofscience</a>
