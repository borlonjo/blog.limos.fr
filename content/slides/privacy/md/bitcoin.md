# Bitcoin

# <i class="fa fa-btc" aria-hidden="true"></i>


## [Bitcoin](https://bitcoin.fr/bitcoin-explique-par-son-inventeur/)

* Satoshi Nakamoto (2009)
  * [Bitcoin White Paper](https://bitcoin.fr/bitcoin-explique-par-son-inventeur/)
    * Monnaie électronique
      * Dématérialisée
      * Finie
    * Entièrement pair à pair (P2P)
    * Résout le problème de la double dépense


## Bitcoin White Paper

* **Objectif** se passer d'une institution bancaire
  * solution décentralisée
* **Solution partielle**
  * système de clés publiques / privées (RSA)
* **Problème à résoudre** la double dépense
  * réseau P2P
    * transactions horodatées, chainées entre elles
      * la chaîne la plus longue est la chaîne nécessitant le plus de calcul à falsifier


## Institution financière

* Tiers de confiance
  * fait la loi dans les transactions

* Interdit les transactions réellement irréversibles
  * les banques jouent le rôle de médiateur

* Le coût de cette médiation est répercutée sur les frais bancaires
  * rend les transactions à faible montant chères


## Non réversibilité

* Si la non réversibilité n'existe pas
  * les vendeurs cherchent à en savoir plus sur leurs clients pour instaurer la confiance

* Le cash règle ce problème
  * mais n'a pas d'équivalent sur un système de télécommuncation
    * <i class="fa fa-hand-o-right" aria-hidden="true"></i> sans autorité centrale


## Preuve cryptographique

* Remplace le tiers de confiance
* Les Transactions
  * directes
  * irréversibles
    * protège le vendeur
    * compte séquestre pour protégér les acheteurs
      * **escrow** internmédiaire pour faire respecter un contrat entre les partis
        * paiement après réception par exemple
          * gère les litiges


## Solution proposée

* Génération d'une preuve calculée de l'ordonnancement des transactions
* Utilisation d'un serveur d'horodatage P2P  
  * distribué sur des noeuds (mineurs)
  * résolvant le problème de la double dépense


## Transactions

* une pièce électronique (*coin*)
  * est une chaîne de signature
* le propriétaire ajoute de la *coin*
  * signature de la transaction précédente
  * clé publique du nouveau propriétaire
* les transactions sont vérifiables par tous


## Transactions

![Transactions](images/bitcoin/transactions.svg)


## Double dépense

* Comment savoir si le propriétaire n'a pas dépensé la coin 2 fois?
  * sans jouer le rôle d'hôtel de la monnaie
    * tout passe par lui
    * seul l'argent qui provient de lui n'est pas dépensé 2 fois
    * solution centralisée


## Solution proposée

* Tout l'historique des transactions sur chaque coin est publique
  * vérifiable
  * distribué
    * consensus pour la version de l'historique à adopter


## Serveur d'horodatage

* *hash* un ensemble d'élément (bloc)
* publication horodaté de ce *hash*
  * ce bloc a existé!
  * avec le *hash* du bloc horodaté précédent

* C'est une chaîne publique de blocs


## Serveur d'horodatage

![Serveur d'horodatage](images/bitcoin/timestamp-server.svg)


## Preuve de Travail / POW

* Challenge
  ```bash
  sha256(X) = 00000004bb4bec2b98.. (32 bytes en hexa soit 64 caractères)
  ```

  * le travail requis croît exponentiellement
    * *f(nombre de 0)*
  * validation simple(un calcul d'empreinte)


## Preuve de Travail / POW

* Il faut refaire le travail pour modifier ce bloc
  * les blocs sont chainés
    * plus un bloc est vieux plus il est coûteux à espérer modifier
      *  la sécurité de la chaîne repose sur ce principe

![Preuve de Travail](images/bitcoin/proof-of-work.svg)


## Preuve de Travail / POW

* Se substitue au vote
  * repose sur la puissance de calcul
* La chaîne la plus longue détient la "vérité"
  * celle qui a nécessité le plus de calcul
* Tant que la majorité de la puissance de calcul est entre les mains de personnes honnêtes
  * les échanges sont sécurisés


## Preuve de Travail / POW

* Difficulté (nombre de 0) ajustée automatiquement
  * objectif de N blocs à trouver par heure en moyenne
  * régule
    * les évolutions de puissance des machines
    * la fluctuation des noeuds de calcul
    * le flux des transactions


## Réseau

* Envoie des transactions par les utilisateurs
* Chaque noeud regroupe les nouvelles transactions dans un bloc
* Chaque noeud travaille à la résolution de la preuve de travail sur son bloc
  * broadcast de la preuve de travail quand elle est trouvée


## Réseau

* Les noeuds vérifient le bloc pour l'accepter
  * la preuve de travail
  * que toutes les transactions du bloc sont valides
  * que toutes les transactions du bloc n’ont pas déjà été dépensées
* Accepter un bloc signifie que le noeud travail sur le bloc suivant
  * <i class="fa fa-hand-o-right" aria-hidden="true"></i> le hash du bloc qui vient d'être validé sera utilisé pour ce nouveau bloc


## Incitation

* La puissance de calcul mise à disposition pour le développement de la chaîne de blocs est récompensée
* La première transaction d'un bloc est une récompense
  * pas d'émetteur
  * le bénéficiaire est le créateur du bloc
    * cela correspond au inage **mining**
* Création monétaire du système


## Incitation

* Contribution des utilisateurs (**fees**)
  * le montant en entrée est plus grand que celui de sortie?
    * la différence est pour le noeud
  * le devenir du système un fois toutes les pièces émises


## SPV

Vérification de Paiement Simplifié

(**Simplified Payment Verification**)

* Le simple utilisateur (wallet) n'a pas besoin de tous les blocs pour créer une nouvelle transaction
  * il peut conserver les derniers blocs de la chaîne la plus longue
    * en interrogeant plusieurs noeuds

* Il lie sa transaction au bloc courant afin de l'horodaté


## Combinaison et Fractionnement de Valeur

* Une grosse entrée ou multiples petites entrées
* Une sortie bénéficiaire et éventuellement une sortie pour récupérer la monnaie
* L'historique complet de chaque transaction est inutile


## Combinaison et Fractionnement de Valeur

![bitcoin](images/bitcoin/combining-splitting-value.svg)


## Confidentialité

* Les clés publiques utilisées pour les transactions sont anonymes
  * les échanges se font entre possesseur des clés
  * on peut générer de nouvelles clés bénéficiaires pour chaque transaction


## Confidentialité

* L'anonymat
  * est levé lors du change en € ou en $
  * les entrées d'une transaction sont contrôlées par la clé en entrée de la transaction

* Lever l'anonymat d'une clé permet de faire parler toutes les transactions associées


## En Pratique

* L'argent est créé différemment
  * création finie à 2100000BTC
* Pas de banque
* Le cluster est coûteux éngergétiquement


## En Pratique

* Transparent
  * grand livre ouvert de transactions publiques
 	* une clé = un billet
		* comme si on pouvait prendre les empreintes digitales de tous ceux qui l'ont eu, datés.
			 on coupe toujours les billets et il y a toujours de la monnaie


## En Pratique

* Pour l'utillisateur c'est du cash!
  * gérer par une wallet
* [Tout ce que vous avez toujours voulu savoir sur Bitcoin](https://lopp.net/bitcoin.html)


## clean (mixers, atomic swaps, trading?)

* mixers: mélangeur de bitcoin
  * on perd le lien aux clés qu'on a
    * on a aucune garanti sur celles que l'on obtient
      * machine à lavers


## Calculer ses fees

* Chaque transaction est plus ou moins complexede  
  * plusieurs payeurs
  * plusieurs destinataires
* les fees sont fonctions de la complexité de la transaction
  * le mineur intelligent choisit en priorité les transactions au meillleur ratio fee/complexité
  * l'utilisateur maximise ses chances d'avoir sa transaction prise en compte rapidement en augmentant les fees


## Calculer ses fees

* Taille transaction = nb_input*148 + nb_output*34 + 10

* transaction simple = 226 bytes

* [Predicting Bitcoin fees for transactions](https://bitcoinfees.earn.com/)

* [Bitcoin Fee Estimation](https://estimatefee.com/)


## Calculer ses impôts

[Impôts & bitcoin : comment bien déclarer ses cryptomonnaies, notre guide en 10 questions](https://www.numerama.com/business/325205-impots-bitcoin-comment-bien-declarer-ses-cryptomonnaies-notre-guide-en-10-questions.html)


## le monde fou fou fou des cryptomonnaies


## White Paper

* 1 cryptomonnaie = 1 White Paper
  * un résumé
    * les problèmes traités
    * la façon de les résoudre
  * éventuellement 1 Yellow paper
    * plus orienté recherche

* [What's the difference between a white paper and a yellow paper](https://www.quora.com/Whats-the-difference-between-a-white-paper-and-a-yellow-paper)


## Buy some

* controle d'identité
  * carte d'identité
  * passeport
  * webcam
  * selfie
* cubtis
* coinbase
* poloniex
