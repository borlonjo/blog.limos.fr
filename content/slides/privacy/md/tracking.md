# Tracking

# <i class="fa fa-user-secret" aria-hidden="true"></i>


## [panopticlick](https://panopticlick.eff.org/)

### Is your browser safe against tracking?

[Electronic Frontier Foundation](https://www.eff.org/about)'s project


## Fingerprint

![Finger print](images/tracking/fingerprint.png)<!-- .element style="width: 80%" -->


## Cookies

* Palie à l'amnésie du protocol HTTP
* En têtes HTTP envoyées par le serveur

  ```http
  Set-Cookie: name=value[; Max-Age=age][; expires=date]
  [; domain=domain_name][; path=some_path][; secure][; HttpOnly]
  ```

* Renvoyées inchangées par le client à chaque requête

  ```http
  Cookie: name=value
  ```

* Cloisonnés par domaine
  * accessibles via les sous domaines


## just do it

* [<i class="fa fa-github" aria-hidden="true"></i> willdurand-edu/cookie-playground](https://github.com/willdurand-edu/cookie-playground)

```bash
git clone https://github.com/willdurand-edu/cookie-playground.git
php composer.phar install
php -S localhost:4000 -t .
```

* tracker exposes a dashboard at: http://localhost:4000/tracker/public/dashboard.
* The website does not do much, but is available at: http://localhost:4000/website/.


## Space cookies

* Techniques de tracking
  * Cookies plus persistants
  * moins limité en taille

* [Supercookie](https://en.wikipedia.org/wiki/HTTP_cookie#Supercookie)
  * cookie de niveau racine (.com)


## Space cookies

* Flash cookie
  * utilise la persistance flash
    * [LSO (Local Shared Object)](https://fr.wikipedia.org/wiki/Objet_local_partag%C3%A9)
      * cross domain
      * accès à l'historique des sites visités utilisant flash


## Space cookies

* [Zombie cookie](https://en.wikipedia.org/wiki/Zombie_cookie)
  * cookie perpétuellement recréé

* [Evercookie](https://en.wikipedia.org/wiki/Evercookie)
  * exploite toute les possibilités
    * [<i class="fa fa-github"></i> samyk/evercookie](https://github.com/samyk/evercookie/)

* [Cookie de tracking / assiste.com](http://assiste.com/Cookie_de_Tracking.html)
* [Using HTML5 Local Storage vs Cookies For User Tracking ...](http://johnsteinmetz.net/blog/using-html5-local-storage-vs-cookies-for-user-tracking/)
