## Piraterie

![intelligence économique](images/jnarac/ie/ie.jpg "intelligence économique")<!-- .element: width="75%" -->


## Motivations des pirates

* Espionnage d'état
* Sécurité d'état
* Intelligence économique
* Argent (chantage/ranson/opération commanditée)
* (H)ac(k)tivisme
* Goût du challenge
* Curiosité / ennui (script kiddies)
* Vengeance
* Pentesting (à titre préventif)


## Social engineering

![chaperon rouge](images/jnarac/ie/chaperon_rouge.jpg "chaperon rouge")<!-- .element: width="45%" -->


## Social engineering

![password facebook](images/jnarac/ie/passwordfacebook.png "password facebook")<!-- .element: width="80%" -->


## le web est piégeable

![Phishing](images/jnarac/www/phishing.png "Phishing")<!-- .element style="width: 70%" -->

* https://www.amazon.fr@bg.box.sk/artcile.htm
  * ne pointe pas vers un article sur amazon.fr


## <i class="fa fa-ambulance" aria-hidden="true"></i> le web est piégeable

* Vérifier l'url dans son navigateur

* Le cadenas vert (https) ne suffit pas
  * obtenir des certificats pour une url malveillante est simple
    * [Let's encrypt](https://letsencrypt.org/)
  * Idéalement regarder les informations du certificat SSL


## Google Fu

![Google Fu](images/jnarac/ie/googlefu.jpg "Google Fu")<!-- .element: width="45%" -->

[Google Hacking Database (GHDB)](https://www.exploit-db.com/google-hacking-database/)

![Google Fu](images/jnarac/ie/google-fu.png "Google Fu")<!-- .element: width="50%" -->


## Shodan HQ

![Shodan HQ](images/jnarac/ie/shodan-hq.png)<!-- .element width="50%" -->

* Découverte de l'internet ...
  * en mode combinatoire
  * en analysant les bannières des services
* Prise en main rapide
  * [Shodan for penetration testers](https://www.defcon.org/images/defcon-18/dc-18-presentations/Schearer/DEFCON-18-Schearer-SHODAN.pdf)
  * [Les dernières requêtes pour l'inspiration](http://www.shodanhq.com/browse/recent)


## IE offensive (pentest)

![Metasploit](images/jnarac/ie/msf.png "Metasploit")<!-- .element width="40%" -->
![Armitage](images/jnarac/ie/armitage.png)<!-- .element width="40%" style="float: right; margin-left: 20px" -->
![Beef](images/jnarac/ie/beef.png)<!-- .element width="40%" -->
![Stuxnet](images/jnarac/ie/stuxnet.jpg)<!-- .element width="40%" style="float: right; margin-left: 20px" -->


## <i class="fa fa-ambulance" aria-hidden="true"></i> Piraterie

* Choisir de [bons mots de passe](passwords.html)
* Mettre à jour
  * son système
  * ses logiciels
* Limiter les informations en ligne
* Sécuriser les systèmes d'information
* Sensibiliser les acteurs des systèmes d'information
