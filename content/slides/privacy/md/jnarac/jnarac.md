## Je n'ai rien à cacher ...

![WC](images/jnarac/index/wc.jpg)

### vraiment rien?
### à vraiment personne?

Note:
- combien considère qu'il n'ont rien à cacher?
- vous fermez la porte quand vous allez aux toilettes?
- vous laissez la webcam perpétuellement allumée?
- je peux avoir les clés de votre habitat?
- vos transactions bancaires m'intéressent aussi
- votre historique de recherche
  - Google
  - facebook  
    - les gens à qui vous pensez le plus
- les gens avec qui vous conversez
  - ce que vous vous dites
- ou vous allez
  - avec qui


## ... donc rien à craindre

![Nothing to hide nothing to fear](images/jnarac/index/nothing_to_hide_nothing_to_fear.jpg)<!-- .element style="width: 30%" -->

### [Novlangue](https://fr.wikipedia.org/wiki/Novlangue)

Note:
- implicitement
  - je n'ai donc rien à craindre
    - puisque je suis du "bon" côté
      - du côté des gens normaux
        - qui correspondent à la norme
- la norme change pour le meilleur
  - l'homosexulalité
  - l'IVG
  - il faut pouvoir penser la transgression pour amorcer l'évolution
- la norme peut à tout moment changer pour le pire
  - pour les juifs
  - pour les étrangers surt un territoire donné
- la normalité n'est pas qu'une question de "volonté"
  - quid d'une personne malade
    - qui souffre ... drogue douce
  - quid des handicapés
    - leur comportement
    - le coup de leur prise ne charge
- attention aux anglicismes, aux nouveaux mots, aux nouveaux emplois


## [le panoptique](https://fr.wikipedia.org/wiki/Panoptique)

![Prison cubaine panoptique](images/jnarac/index/prison-cubaine-panoptique.jpg)<!-- .element style="width: 85%" -->

Note:
- le panoptique est un modèle carcéral
  - accepter la surveillance de masse
    - c'est légitimé cette architecture dans le monde libre
      - et donc annihiler la liberté de parole et de penser
        - celui qui est dans la tour à le pouvoir sur les autres


![Elf Surveillance Santa Camera](images/jnarac/index/Elf_Surveillance_Santa_Camera.png)

![Edward Snowden](images/jnarac/index/twitt-snowden.png)


![Degrés de séparation](images/jnarac/index/degres-separation.png)<!-- .element style="width: 50%" -->

Famille, amis, collègues, entreprises, institutions ... tout est imbriqué!
