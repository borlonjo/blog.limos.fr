# X.509 / TLS

# <i class="fa fa-user-secret" aria-hidden="true"></i>


### OpenSSL

* Implémenté en C
* Boîte à outils de chiffrement
  * bibliothèques cryptographie générale
  * bibliothèques implémentant le protocole SSL
  * commande en ligne
* Supporte SSL 2.0, SSL 3.0 et TLS 1.0
* Actuellement TLS 1.3
  * [ANSSI : privilégier TLS1.3 et tolérer TLS1.2, TLS1.1 et TLS1.0](https://www.ssi.gouv.fr/uploads/2016/09/guide_tls_v1.1.pdf)
* Distribué sous une licence de type Apache


### GnuTLS

* Conforme aux spécifications de l'IETF
* Permet l'authentification via les certificats
  * X509
  * PGP
* A la différence d'OpenSSL, GnuTLS est compatible avec les licences GPL


### [X.509](https://fr.wikipedia.org/wiki/X.509)

* Norme Pour
  * certificats à clé publique
  * listes de révocation de certificats
  * attributs de certificat
  * algorithme de validation du chemin de certification

* UIT (agence de l'ONU le développement spécialisé dans les TIC )
  * v1: 1988, v2: 1993, v3: 1996


## [X.509](https://fr.wikipedia.org/wiki/X.509)

* Système hiérarchique d'autorités de certification
  * *certification authority* - ***CA***
    * une ***CA*** attribue un certificat liant
      * une clé publique
      * un nom distinctif
        * *Distinguished Name* - ***DN***


## [X.509](https://fr.wikipedia.org/wiki/X.509) DN

* *C*: Country
* *L*: Locality
* *ST*: State
* *O*: Organisation
* *SO*: Organizational Unit
* *CN*: Common Name
* *Street*: Adress
* *E*: Mail


#### Anatomie

* **Version de la norme**
* **Serial**
* Algorithme de signature du certificat
* **Issuer** le signataire (***DN*** de la ***CA***)
* **Validity** début fin de validité
* **Subject name** **DN** identifié par le certificat
* **Subject Public Key**
* Extensions (ajouté en v3)
  * paires clé / valeur

Le tout signé par la **CA**


#### Extensions

Informations ou contraintes d'utilisation
* **Key Usage**
* **Subject Key Identifier**
* **keyEncipherment** Algorithme de signature du certificat
* **rfc822Name** email address
* **dNSName** DNS name for a machine
* **uniformResourceIdentifier** URL
*  **iPAddress**


## Générer une clé RSA

```bash
$ openssl genrsa -out ca.key 4096
```

* 4096 représente la taille de la clé
* ca.key contient la clé privée ET la clé publique

```bash
$ openssl rsa -in ca.key -pubout
```

* Permet d'extraire la partie publique uniquement


## Générer un certificat

```bash
$ openssl req -new -x509 -days 1826 \
            -key ca.key -out ca.crt \
            -subj '/CN=m4z3.me'
```

```bash
$ cat ca.crt
-----BEGIN CERTIFICATE-----
MIIC/zCCAeegAwIBAgIJAM4FANszQweWMA0GCSqGSIb3DQEBCwUAMBYxFDASBgNV
BAMMC2V4YW1wbGUuY29tMB4XDTE3MTAzMTExMzEzNFoXDTE3MTEzMDExMzEzNFow
```

[différences entre certificats](https://www.sslmarket.fr/ssl/help-la-difference-entre-certificats)


## Certificat auto-signé

* **Issuer** et **Subject** identiques
* Tout le monde peut en fabriquer
* Rejetés par défaut par les navigateurs


#### Faire parler un certificat

local

```bash
$ openssl x509 -text -noout -in ca.crt
```

distant

```bash
$ openssl s_client -connect isima.fr:443 -showcerts -servername isima.fr
```

* [Using OpenSSL’s s_client command with web servers using Server Name Indication (SNI)](https://major.io/2012/02/07/using-openssls-s_client-command-with-web-servers-using-server-name-indication-sni/)

usage du certificat

```bash
$ openssl x509 -purpose -in ca.crt  -inform PEM
```


## Faire parler un certificat

![Check CERT Firfox](images/ssl/check-cert-ff.png)

* Avec son navigateur en cliquant sur le cadenas


## Faire parler un certificat

![Check CERT Firfox](images/ssl/check-cert-ff2.png)


## Autorité de certificaiton (CA)

* Tiers de confiance
* Recueille les demandes de certifications
  * vérifie la validité de la demande
    * vérifie l'identité
      * preuve par contrôle des domaines
* Signe les certificats
* Gère les révocations


## Autorité de certificaiton (CA)

* ***CA*** de confiance  
  * importées par défaut dans le navigateur
    * Tout supprimer?
  * être importée dans les navigateurs
    * payer (le navigateur)


## Known good signers

![Known good signers](images/ssl/known-good-signers.png)


## [Root CA](https://fr.wikipedia.org/wiki/Certificat_racine)

Certificat racine

* Clés publiques non signées, ou auto-signées
  * le sommet de la pyramide de confiance  
  * un certificat est rarement signé par une ***CA*** racine
  * la ***CA*** racine créée plusieurs ***CA*** intermédiaires
    * sous scellés / déconnectés / sortis (autre ***CA***)
    * auto signé

[DigiCert Trusted Root Authority Certificates](https://www.digicert.com/digicert-root-certificates.htm)


## Chain of trust

Chaînes de certification

* Les ***CA*** intermédiaires signent
  * les certificats des clients
  * d'autres ***CA*** intermédiaires
    * il faut alors fournir la chaîne de certification
      * au cas où l'intermédiaire ne soit pas dans le navigateur


## CA connues

* [digicert](https://www.digicert.com/)
  * [verisign](https://www.websecurity.symantec.com/fr/fr/ssl-certificate)
* [Comodo](https://www.comodo.com/)
* ...
* Gratuites
  * [StartSSL free](https://www.startcomca.com/index/support?v=1)
  * [CAcert](http://www.cacert.org/) (not known good signers)
  * [Let's Encrypt](https://letsencrypt.org/) fondé par l'[EFF](https://www.eff.org/fr) et [Mozilla](https://www.mozilla.org/fr/)


## CA & Firefox

![Warning](images/ssl/https-ff.png)


## CA & Chrome

![Warning](images/ssl/https-chrome.png)


## Différentes causes

* Certificat différent du nom de domaine
  * wildcard
    * https://amendes.gouv.fr
* Certificats expirés
* ***CA*** non importée
* ...


## Certificate Sign Request
### (CSR)

```bash
$ openssl req -new -newkey rsa:2048 -sha256 \
            -nodes -out user.csr -keyout user.key \
            -subj '/CN=example.com'
```

* Générer un requête de certification
* Un ***CSR*** est auto-signé (pour vérifier l'intégrité)

```bash
$ openssl req -in user.csr -text -noout
```

* Lire le ***CSR***

```bash
$ openssl req -text -noout -verify -in user.csr
```

* Vérifier le ***CSR***


### Création d'un certificat à partir d'un CSR

```bash
$ openssl x509 -req -days 365 \
       -CA ca.crt -CAkey ca.key \
       -CAcreateserial -CAserial serial \
       -in user.csr -out user.crt \
```

* Génèrer un certificat à partir d'un ***CSR***
  * la ***CA*** vérifie qu'elle gère le domaine
  * la ***CA*** ajoute quelques informations
  * la ***CA*** signe avec sa clé privée
    * la ***CA*** protège sa clé privée

note:
- https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs


## Vérification de la signature

```bash
$ openssl verify -CAfile ca.crt user.crt
user.crt: OK
```

```bash
$ openssl x509 -text -noout -in user.crt
Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            f0:61:f4:c1:96:86:21:07
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN=m4z3.me
        Validity
            Not Before: Jan 18 17:27:54 2018 GMT
            Not After : Jan 18 17:27:54 2019 GMT
        Subject: CN=example.com
```
note:
- https://stackoverflow.com/questions/25482199/verify-a-certificate-chain-using-openssl-verify


## Signer un fichier

[Sign and verify text files to public keys via the OpenSSL Command Line](https://raymii.org/s/tutorials/Sign_and_verify_text_files_to_public_keys_via_the_OpenSSL_Command_Line.html)


#### Certificate Revocation List (CRL)

Révocation de certificats

* Contient une liste de certificats valides à révoquer
  * utile en cas de compromission / décommissionnement
    * informer la ***CA***
    * la ***CA*** ajoute le certificat à sa liste de certificats révoqués
    * cette liste est signée par la ***CA***
* Souvent la clé qui signe les certificats signe les ***CRL***
* Quand que le navigateur interroge-t-il les ***CRL*** ?


####  Certificate Revocation List (CRL)

* Serial
* Algorithme de signature de la ***CRL***
* **Issuer** le signataire (***DN*** de la ***CA***)
* **Update date **
* **Next Update date**
* **CRL**
  * revoked 1
  ...
  * revoked 1
* Extensions
  * paires clé / valeur

note:
- next update parce que CRL delta


## Revocation reason

* unspecified (0)
* keyCompromise (1)
* CACompromise (2)
* affiliationChanged (3)
* superseded (4)
* cessationOfOperation (5)
* certificateHold (6)
* removeFromCRL (8)
* privilegeWithdrawn (9)
* AACompromise (10)


#### Online Certificate Status Protocol (OCSP)

Protocole d’interrogation de validité pour un certificat

![OCSP](images/ssl/OCSP.png)

* si l'***OCSP*** n'est pas disponible pour le certificat firefox accepte le certificat
  * s'il est valide


#### Online Certificate Status Protocol (OCSP)

* Peu déployé
* [Approche par log publique de création révocation](http://confiance-numerique.clermont-universite.fr/Slides/R-Sasse.pdf) [<i class="fa fa-video-camera"></i>](http://webtv.u-clermont1.fr/media-MEDIA150907102804168)
* [Google's Certificate Transparency project](http://www.certificate-transparency.org/)


## <strike>SSL</strike> / TLS

* Crée un canal de communication **authentifié**, protégé en **confidentialité** et en **intégrité**
* Utilise des certificats X.509
  * délivrés par des ***CA***
* Utilise un système de chiffrement asymétrique
  * pour échanger une clé pour le chiffrement symétrique
* Protocole initialement pensé pour sécurisé HTTP
  * étendu à d'autres services (SMTP, LDAP, VPN, ...)


## TLS dans le modèle TCP/IP

![SSL/TLS in TCP/IP model](images/ssl/tcp-ip_model_ssl-tls_protocol.png)

* Couche intermédiaire car indépendante du protocole utilisé


## Versions SSL

Secure Socket Layer

* **1.0** par Netscape en 1994, pas de public release
* **2.0** par Netscape en Février 1995, [The SSL Protocol Version 2.0](http://www.frameip.com/rfc/draftxxx.php)
* **3.0** par Netscape en Novembre 1996, [The SSL Protocol Version 3.0](http://www.frameip.com/rfc/draft302.php)


## Versions TLS

Transport Layer Security

* **1.0** released en janvier 1999, [RFC 2246](http://www.frameip.com/rfc/rfc2246.php)
  * **TLS 1.0** = SSL 3.1 [IETF](http://www.ietf.org/)
* **1.1** released en Avril 2006, [RFC 4346](http://www.frameip.com/rfc/rfc3546.php)
* **1.2** released en Août 2008, [RFC 5246](http://www.frameip.com/rfc/rfc4366.php)
* **1.3** released en Août 2018, [RFC 8446](https://www.bortzmeyer.org/8446.html)


## [Cypher suite](https://fr.wikipedia.org/wiki/Suite_cryptographique)

Suite cryptographique

* échange de clés
* authentification des parties
* chiffrer les données applicatives
* protéger les données applicatives en intégrité (MAC)

**Cipher suites** gérées par un système

<pre><code class="hljs bash" style="font-size: 35px">$ openssl ciphers -v</code></pre>


## TLS_RSA_WITH_RC4_128_MD5

  Se lit

* [RSA](https://fr.wikipedia.org/wiki/Chiffrement_RSA)
  * authentification du serveur
* RC4_128 = algorithme de chiffrement [RC4](https://fr.wikipedia.org/wiki/RC4) avec clé de 128 bits
  * pour chiffrer le canal de communication
* [MD5](https://fr.wikipedia.org/wiki/MD5)  
  * protection de l'intégrité du canal de communication  via [HMAC MD5](https://fr.wikipedia.org/wiki/Keyed-Hash_Message_Authentication_Code)


![TLS Handshake](images/ssl/TLS-Handshake.png "SSLv2 Handshake")


## clienthello

Le client

* envoie les suites cryptographiques qu'il est capable de mettre en oeuvre


## Réponse serveur

Le serveur
  * Si aucune suite n'est satisfaisante
    * **Alert** fin de connexion
  * Sinon
    * **ServerHello** suite cryptographique choisie
    * **ServerRandom** valeur aléatoire (publique)
    * **Certificate** certificat du serveur
    * **ServerHelloDone** indiquant qu'il est attente d'une réponse du client


#### Négociation de la suite cryptographique

* Tous les serveurs web ne négocient pas les cypher suites de la même façon
  * **IIS** prend la première suite cryptographique préférée du serveur et proposée par le client
  * **Apache** prend la première suite proposée par le client et supportée par le serveur
    * permet une baisse du niveau de sécurité
    *  *SSLHonorCipherOrder* pour sécuriser

[SSL / TLS Renegotiation Handshakes MiTM Plaintext Data Injection- medium or low      risk?](https://security.stackexchange.com/questions/63867/ssl-tls-renegotiation-handshakes-mitm-plaintext-data-injection-medium-or-low)


## Fin de négociation

Le client

  * Vérifie la validité du certificat
  * Génère une clé de chiffrement symétrique  
    * **secret partagé** ou **pre master secret**
  * Chiffre le **secret partagé** avec la clé publique du serveur


## Fin de négociation

Le client envoie au serveur

  * **ClientKeyExchange** le **secret partagé** chiffré
  * **ChangeCipherSpec** paramètres pour la suite cryptographique choisie
  * **Finished** fin de négociation
    * premier message chiffré symétriquement
      * avec **secret partagé** et **ServerRandom**


## Fin de négociation

Le serveur

  * Reçoit le **secret partagé** chiffré généré par le client
  * Déchiffre le **secret partagé** avec sa clé privée
  * **ChangeCipherSpec** paramètres pour la suite cryptographique choisie
  * **Finished** fin de négociation

<br />

#### La suite de la communication est chiffrée symétriquement


## Connexion SSL/TLS

* Le client a authentifié le serveur
  * mais le serveur n’a aucune information sur le client
    * possibilité d'avoir un certificat côté client

* Les paramètres de chiffrements
  * sont négociés et « jetables »


## Certificats clients

* Un client peut présenter un certificat au serveur
* Le serveur vérifie si le certificat est signé par une CA de confiance
* Le serveur peut utiliser ces informations pour authentifier l’utilisateur
* Le certificat peut être stocké dans un périphérique (e.g. [Yubikey](https://www.yubico.com/)), une carte à puce (e.g. [CPS](https://fr.wikipedia.org/wiki/Carte_de_Professionnel_de_Sant%C3%A9)), ...


## Vulnérabilité TLS_RSA_WITH_RC4_128_MD5

* si la clé privée du serveur est récupérée
  * le **secret partagé** est récupérable
    * on peut obtenir les clés de session
      * toutes les communications sont alors déchiffrables
        * passées
        * futures


## Perfect Forward Secrecy (PFS)

*Confidentialité Persistante*

* La clé compromission d'un clé privée n'affecte pas la confidentialité des communications passées
  * utilisée uniquement pour signer
* [Problème NP-complet](https://fr.wikipedia.org/wiki/Probl%C3%A8me_NP-complet)


![Diffie-Hellman exchange](images/ssl/diffie-hellman-exchange.png "Diffie-Hellman exchange")


## Certification Authority Authorization (CAA)

* Une CA peut vérifier si elle est autorisée à émettre un certificat pour un domaine via le DNS (enregistrement CAA)

* Devenu obligatoire le 8 septembre 2017

* Le 9 septembre 2017, Comodo s’est fait pincer pour ne pas le respecter :
[Comodo Caught Breaking New CAA Standard One Day After It Went Into Effect](https://www.bleepingcomputer.com/news/security/comodo-caught-breaking-new-caa-standard-one-day-after-it-went-into-effect/)

Note:
- cas où un domaine est déjà enregistré chez un CA (let's encrypt)
  - et qu'une autre CA lui délivre un certificat
  - Comodo n'en a pas tenu compte


## DNS-Based Authentication of Named Entities (DANE)

* pour un nom de domaine
  * publication du certificat signé dans un enregistrement TLSA du DNS
    * protégé par DNSSEC
  * permet de spécifier quelles **CAs** peuvent émettre des certificats

* Validation par les clients


## HTTPS

* HTTP + SSL/TLS = HTTPS assure
  * Confidentialité
    * [Session Hijacking](http://en.wikipedia.org/wiki/Session_hijacking)
      * [les dangers du wifi](https://wiki.wireshark.org/CaptureSetup/WLAN)
        * [firesheep](http://codebutler.github.io/firesheep/)
  * Intégrité
  * Authentification
    * via les certificats


## Apache

* __Open SSL__
  * mod_ssl
* __GnuTLS__
  * [mod_gnutls](https://technique.arscenic.org/lamp-linux-apache-mysql-php/apache-le-serveur-http/modules-complementaires/article/installer-et-configurer-le-module)


## Que "chiffre" https

* On ne voit pas l'url dans le traffic
  * on voit l'ip de l'hôte
  * on voit ventuellement les requêtes DNS
* Proxy https ou VPN anonymisent complètement le traffic

Note:
- Attention les proxy
  - surtout anonymes sont de faux amis
  - ce n'est pas un vpn
  - pose des problèmes de certificats
    - proxy https = MITM


## Vulnérabilités multiples

* [2015] [Weak Diffie-Hellman and the Logjam Attack](https://weakdh.org/)
  * permet de forcé les connexions TLS à 512-bit export-grade cryptography
  * [Freack Attack](https://freakattack.com/) réminiscence
* [2014] [Poddle](http://www.dwheeler.com/essays/poodle-sslv3.html)
  * [POODLE test](https://www.poodletest.com/)
* [2014] [Heartbleed](https://fr.wikipedia.org/wiki/Heartbleed)
  * lecture de la mémoire du serveur via un heartbeat


## Vulnérabilités multiples

* [OpenSSL vulnerabilities](https://www.openssl.org/news/vulnerabilities.html)
* [GnuTLS security](http://www.gnutls.org/security.html)
* [Les attaques SSL / TLS](https://korben.info/les-attaques-ssltls.html)
* [Public Key Infrastructure (PKI)](https://fr.wikipedia.org/wiki/Infrastructure_%C3%A0_cl%C3%A9s_publiques)
  * [Certificate authorities issue SSL certificates to fraudsters](http://news.netcraft.com/archives/2015/10/12/certificate-authorities-issue-hundreds-of-deceptive-ssl-certificates-to-fraudsters.html)  


## <i class="fa fa-gears"></i> Tests serveurs

* Tester un certificat SSL
  * [SSL Decoder](https://ssldecoder.org/)
  * [Certificate Expiry Monitor](https://certificatemonitor.org/)
* Tester une configuration SSL
  * [Qualys](https://www.ssllabs.com/ssltest/)
  * [Comodo ssl analyzer](https://sslanalyzer.comodoca.com/)
  * [OpenSSL Decoder](https://raymii.org/s/software/OpenSSL_Decoder.html)
  * [Strong SSL Security On nginx](https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html)
  * ```shell
    $ nmap -p 443 --script=ssl-enum-ciphers heart.bleed
    ```


## <i class="fa fa-gears"></i> Tests navigateurs

* [SSL Cipher Suite Details of Your Browser](https://cc.dcsec.uni-hannover.de/)
* [How's my SSL?](https://www.howsmyssl.com/)
* [<i class="fa fa-firefox"></i> Toggle Cipher Suites](https://addons.mozilla.org/fr/firefox/addon/toggle-cipher-suites/)
  * [<i class="fa fa-github"></i> Toggle Cipher Suites](https://github.com/dillbyrne/toggle-cipher-suites/releases)
* [<i class="fa fa-github"></i> Calomel SSL validator](https://addons.mozilla.org/fr/firefox/addon/calomel-ssl-validation/)

Note:
- aspect arbitraire de la notation notamment qualys
  - méfiance quand il y aquelque chose à vendre
- SSL en fait TLS on est d'accord


## <i class="fa fa-medkit"></i> Se protéger

* un service sans **s** est un problème
  * pas ftp, mais sftp ou ftps
  * pas rsync, mais rsync over sssh
  * pas imap, pop3 et smtp, mais imaps, pop3s et smtps
* Seules les implémentations conformes à TLSv1 et supérieures doivent être employées
* Les cyphersuites offrant la PFS doivent être favorisées
* [Anssi - SSL/TLS: état des lieux et recommandations](https://www.ssi.gouv.fr/uploads/2012/06/ssl_tls_etat_des_lieux_et_recommandations.pdf)


## <i class="fa fa-medkit"></i> Se protéger / Apache

* [Chiffrement fort SSL/TLS : Mode d'emploi](https://httpd.apache.org/docs/2.4/fr/ssl/ssl_howto.html)
* [Hardening Your Web Server’s SSL Ciphers](https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/)
* [ssllabs.com's own Apache SSL Config Directives](https://community.qualys.com/thread/9652)
* [Apache web server SSL best practices](https://wiki.fysik.dtu.dk/it/SSL_best_practices)


## <i class="fa fa-medkit"></i> Se protéger / Nginx

* [HTTPS on Nginx: From Zero to A+ (Part 1)](https://juliansimioni.com/blog/https-on-nginx-from-zero-to-a-plus-part-1/)
* [HTTPS on Nginx: From Zero to A+ (Part 2)](https://juliansimioni.com/blog/https-on-nginx-from-zero-to-a-plus-part-2-configuration-ciphersuites-and-performance/)


#### <i class="fa fa-medkit"></i> Se protéger / tout serveur

* [https://cipherli.st/](https://cipherli.st/) pour une conf sécurisée
  * [<i class="fa fa-warning"></i> Modifier tous les vhosts pour nginx!!](http://serverfault.com/questions/641150/nginx-cant-disable-sslv3)

* fixer le [weak Diffie-Hellman (aka logjam Attack](https://weakdh.org/))
  <pre><code class="hljs bash" style="font-size: 28px"> openssl dhparam -out dhparams.pem 2048 </code></pre>

* suivre les [<i class="fa fa-book"></i> recommandations de l'ANSSI](https://www.ssi.gouv.fr/agence/publication/ssltls-3-ans-plus-tard/)


#### A lire

* [Déroulement des échanges ssl en détail](https://www.securiteinfo.com/cryptographie/ssl.shtml)
* [Comprendre SSL/TLS - 1, 2, 3, 4, 5](https://blog.eleven-labs.com/fr/comprendre-ssl-tls-partie-1/)
* [CaenCamp #33 : Infrastructures à clés publiques](https://www.youtube.com/watch?v=9zNAUFtw7Ac) par [Romain Tartiaire](https://romain.blogreen.org/)
* [Chrome, Firefox et recherches Google : passage en force du HTTPS ](http://dareboost.developpez.com/tutoriels/securite-web/https-nouveaute-recherche-google-chrome-firefox/)
* [http://cypherpunks.to/~peter/T2a_X509_Certs.pdf](http://www.cypherpunks.to/~peter/T2a_X509_Certs.pdf)
