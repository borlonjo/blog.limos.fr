## SSI

![SSI](images/jnarac/ssi/vigipirate.jpg "SSI")<!-- .element width="40%" -->


### Problématique nationale

#### [La défense en profondeur](http://circulaire.legifrance.gouv.fr/pdf/2009/04/cir_2014.pdf)

![fortification à la Vauban](images/jnarac/ie/Vauban_Fortifications.jpg "fortification à la Vauban")<!-- .element width="45%" style="float: right;margin: 15px;"-->

exploiter plusieurs techniques de sécurité afin de réduire le risque lorsqu'un composant particulier de sécurité est compromis ou défaillant

<small>Sébastien Le Prestre de Vauban</small>


## Stratégie de la défense en profondeur

* n'évite pas l'attaque
  * ralentit l'attaquant
    * chacun est un maillon des chaînes fonctionnelles Sécurité Défense & SSI et doit

<br>

### Sécurité = réduire le risque = rendre les attaques coûteuses


### Stratégie de la défense en profondeur

Chaque maillon est reponsable

* de l'analyse des risques inhérents à son périmètre pour mieux les maîtriser
* de l'anticipation & de la prévention des accidents et des actes de malveillance
* de l'amélioration continuelle de la sécurisation de son périmètre
  * le risque 0 n'existe pas
  * la sécurité peut toujours être améliorée


## la chaîne Sécurité Défense

![la chaîne Sécurité Défense](images/jnarac/ie/chaine_fonctionnelle.png "la chaîne Sécurité Défense")<!-- .element width="80%" -->


## la chaîne SSI

![Organisation nationale](images/jnarac/ie/organisation_nationale.png "Organisation nationale")<!-- .element width="45%" -->


## l'ANSSI

[![ANSSI](images/jnarac/ie/Anssi.png "ANSSI")](http://www.ssi.gouv.fr/)


## l'ANSSI

* force d'intervention (CERT-FR) & de prévention
* contribue à l'élaboration de la stratégie nationale et européenne SSI
  * [EBIOS Expression des Besoins et Identification des Objectifs de Sécurité](https://www.ssi.gouv.fr/guide/ebios-2010-expression-des-besoins-et-identification-des-objectifs-de-securite/)
  * [Livre blanc sur la sécurité et la défense nationale](http://www.livreblancdefenseetsecurite.gouv.fr/)
    * renforcé par la LPM
      * protège 218 [OIV (Opérateurs d'Importance Vitale)](http://fr.wikipedia.org/wiki/Op%C3%A9rateur_d'importance_vitale) en France


#### ... d'importance Vitale

[SAIV (Secteur d'Activités d'Importance Vitale)](http://www.sgdsn.gouv.fr/site_rubrique70.html) - selon article R1332-2 du Code de la défense français

* **Secteurs étatiques**: activités civiles de l’Etat, activités militaires de l’Etat, activités judiciaires
* **Secteurs de la protection des citoyens**: santé, gestion de l'eau, alimentation
* **Secteurs de la vie économique et sociale de la nation**: énergie, communication, électronique, audiovisuel et information (les quatre représentent un secteur), transports, finances, industrie
* La liste exhaustives est secret défense
