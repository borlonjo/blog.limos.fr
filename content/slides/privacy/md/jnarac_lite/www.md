## Internet

![Hippies](images/jnarac/www/hippies-60s.jpg)<!-- .element width="60%" -->


## Internet

* __efficient__: les messages arrivent toujours

* __résiliant__: trouve d'autres chemin si besoin

* __projet politique__: modèle de société mondiale

* __décentralisé__: personne ne le contrôle réellement

* __ouvert__: très facile de s'y connecter

* __nativement non sécurisé__: tout cricule en claire

* __transmission par paquet__: via le protocol TCP / IP


## le web

* n'est pas Internet, mais fonctionne grâce à l'Internet
* n'est ni Google, ni facebook
  * encore moins chrome ou firefox
* inventé par [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> Tim Berners Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee) début 90
  * il est conçu comme une mine d'informations
    * pas comme un lieu de transactions économiques
      * tout est ouvert
        * [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> Wikipédia](https://wikipedia.org) incarne les concepts fondamentaux du web


## le web

* repose sur le parcours discursif
  * lien hypertexte
    * [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> URL (Uniform Resource locator)](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator)

<br />

<div style="text-align: center">
![url](images/jnarac/www/url.jpg "url")<!-- .element: width="55%" -->
</div>

* supporté par un protocole [HTTP](../1337/http.html)


## le web n'oublie jamais

[![wayback machine](images/jnarac/www/waybackmachine.png "Waybackmachine")](http://web.archive.org/web/20020331020421/http://vmazenod.free.fr/)

Note:
  - waybackmachine vous connaissez?
  - le tout est d'assumer ses coupes de cheveux
  - et ses propos antérieurs, on change, l'environnement change


## <i class="fa fa-ambulance" aria-hidden="true"></i>  le web n'oublie jamais

* Désactiver les partages automatiques

  * de localisation
  * de photos

* Réfléchir avant de mettre quoique ce soit en ligne
  * photos
  * partages d'information

* Se protéger et protéger les autres
  * Enfants, famille, amis, collègue, employeurs


## le web est résiliant

[![Effet Streisand](images/jnarac/www/Streisand_Estate.jpg "Effect Streisand")](http://fr.wikipedia.org/wiki/Effet_Streisand)

Note:
- effet Streisand en 2003
    - poursuite du photographe diffuseur
        - 420 000 visistes le mois suivant
            - l'image sur wikipedia en Creative common
                - ce qu'on essaie de supprimer peut rester


## <i class="fa fa-ambulance" aria-hidden="true"></i> le web est résiliant

* Faire valoir [son droit au déréférencement](https://www.cnil.fr/fr/le-droit-au-dereferencement)
  * suppression des résultats des moteurs de recherche
    * ne supprime pas l'information du web
* [Google Search Console (ex webmaster tools)](https://www.google.com/webmasters/tools/home?hl=fr&pli=1)
  * si vous avez "la main sur la page"
