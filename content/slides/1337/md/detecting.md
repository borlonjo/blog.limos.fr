# détecter

# <i class="fa fa-puzzle-piece" aria-hidden="true"></i>


### On cherche des points d'entrée **sur** ou **vers** la cible

!["scanning"](images/detecting/mapping.png "scanning")<!-- .element width="80%" -->


## Architecture

* **Virtual hosting** / **hébergement mututalisé**
  * éventuellement connu en phase de reconnaissance
    * via les DNS
    * via les moteurs de recherche (bing)
  * se connecter au serveur par défaut


## Architecture

* **Reverse proxy**
  * un proxy type [nginx](https://www.nginx.com/) cache le serveur a qui il renvoie le traffic
    * il est parfois possible d'atteindre le serveur directement


## Architecture

* **Load Balancing**
  * les injections peuvent avoir des résultats obfusqués
  * on peut tenter de forcer pour cibler un serveur de sortie
  * l'en tête http Date peut révéler des serveurs non synchronisés

Note:
- virtual hosting accessible par la racine (cf exo)
- load balancing des paramètres peuvent se trouver dans
    - la query string
    - les cookies


#### (Server / service) (profiling / fingerprint)

* Ports ouvert
* Bannières
* Version de l'OS    
* Version de services
    * TLS / SSL
* en-tête http

<pre><code>Server:  Apache</code></pre>


#### (Server / service) (profiling / fingerprint)

* Pages par défaut
* Pages d'erreurs
* On peut avoir des versions vulnérables mais patchées
    * cas de la version de PHP par défaut
        * liée à la version de Debian

Note:
- comme on a l'archi ca vaut le coup de regarder sur tous les serveurs connus
- les numéros de versions facilitent la recherhce de vulnérabilité / exploit
    - openssl 1.0.1 = Heartbleed
    - Drupal 7.x drupalgeddon
- page par défaut apache (it works) silencieuse
    - pas forcément le cas d'IIS
- les 404 par défaut d'apache donne la version d'apache


## [nmap](https://nmap.org/)

<pre><code>nmap 172.16.76.0/24</code></pre>

#### Recherche des hôtes et des services
* -O pour la signature de l'OS
  * envoie des paquets malformés pour déterminer
  * la malformation des paquets réseaux bas niveau nécessite les droits de root


## [nmap](https://nmap.org/)

* -sV détection de la version des services
  * teste le service attendu
  * teste d'autres options si ça rate
  * à base d'envoi de paquets
    * nudge packets - malformés
* -p spécifie le port / le range de port
* -p- TOUS les ports


## [nmap](https://nmap.org/)

* -sC passe les scripts non intrusifs
  * par défaut tous le scan des wellkown ports 1024
* scriptable --script=script-name
    * &ast;.nse écrit en lua (à vérifier)
        * /usr/share/nmap/scripts/http-robots.txt.nse
    * pour samba, quake, vns, sip, ... plein
* possibilité d'export formaté des résultats
    * réutilisable en entrée de certains outils

Note:
- [nmap service probes](https://nmap.org/book/vscan-fileformat.html)
    - détection de services custom
- autre scanners
    - [openvas](http://www.openvas.org/)
    - [nessus](http://www.tenable.com/products/nessus-vulnerability-scanner)
    - [w3af](http://w3af.org/)
    - [ZAP](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project) en scanner de vulnaérbilité


## Analyse de code / client side

* Signatures de CMS
  * noms et valeurs d'attributs HTML
  * champs de formulaire
  * nom de class css
* Tous les commentaires
  * notes de développeur
  * documentation de fonctions
  * vieux codes commentés (liens cachés ou oubliés)
    <pre><code data-stream class="hljs php">// $pass = MD5($mdp + $birthdate)</code></pre>
    <!-- a href="http://pastebin.com/mNdmD5L2">voir aussi luckypada1</a -->

Note:
- libs framework compliqué à détecter
    - symfony 1.0 environnement par exemple
        - dev.php
        - page de configuration
            - assez bien fait maintenant
    - facile pour js
- récupérer par Nikto


## urls de l'appli

* [drupal](https://www.drupal.fr/)
  * /?q=node/234
  * /user
* [WordPress](https://fr.wordpress.com/)
  * /?p=25
  * /wp-admin
* [spip](https://www.spip.net)
  * /spip.php?article765
  * /ecrire        
* url rewriting ([mod_rewrite](https://httpd.apache.org/docs/current/fr/mod/mod_rewrite.html))


## path systèmes de l'appli

* messages d'erreurs
  * [MySQL](https://dev.mysql.com/doc/refman/5.7/en/error-log.html) / [php](http://php.net/manual/fr/function.error-reporting.php)
* [phpinfo](http://php.net/manual/fr/function.phpinfo.php)
  * également version OS, PHP, lib
* facilitent
  * [injection de commande](command_execution.html)
  * inclusion ([RFI/LFI](lfi_rfi.html))


## Automatic detection

* [Wappalyzer](http://wappalyzer.com)
* [BlindElephant Web Application Fingerprinter](http://blindelephant.sourceforge.net/)
    * [Supporter par Qualys](http://www.qualys.com/blindelephant)
* <i class="fa fa-github"></i> [Dionach/CMSmap](https://github.com/Dionach/CMSmap)
* [CMS Detector](http://guess.scritch.org/)
* [CMS explorer](https://hackertarget.com/cms-explorer/)
* [Ermis CMS Security Scanner](https://addons.mozilla.org/fr/firefox/addon/ermis-cms-security-scanner/)
* spécialisés
    * [WPScan](http://wpscan.org/)
        * inclus les plugins vulnérables


## Gestion de sessions

* [SID](http://php.net/manual/fr/session.idpassing.php)
  * copier le SID ouvre une session
  * /server-status [mod_status](https://httpd.apache.org/docs/2.4/mod/mod_status.html)
* cookie
  * copier le cookie ouvre une session
  * regarder s'il y d'autres paramètres
* SID prédictibles?
  * patterns triviaux
  * la recherche de non [entropie](https://fr.wikipedia.org/wiki/Entropie_de_Shannon)
  * tomber sur une session en cours

Note:
- paramètre dans cookie cf TP
- [Entropy analysis tools](http://sourceforge.net/p/entropyanalysis/code/ci/master/tree/)
- [Entropy analysis tools](https://github.com/ulikoehler/entropy-analysis-tools)
    - toujours le même résultat : entropie nulle


## Spidering

* Arborescence
  * cartographie
    * des ressources
    * des fonctionnalités
  * pages dynamiques
    * risque de boucle infini
    * profondeur (scope)


## Spidering

* Répertoires "cachés"
  * directory browsing
  * 403
    * le répertoire existe
  * Per-user web directories (public_html)    
  * parties inutilisées mais toujours active
    * `/old`


## Spidering

* Fichiers "cachés" (non directement liés)
  * aides en ligne
  * manuels de configuration, connexion    
  * page d'admin (back office, phpMyAdmin, ...)


## Spidering

* Affichage de configuration en clair
  * extensions de fichiers renvoyés en clair
    * `.txt, .yml, .xml, ...`
  * mauvaise configuration de l'interpréteur PHP
    * les fichiers php sont renvoyés en texte
  * fichiers de sauvegardes oubliés
    * `/save/backup.zip`


## Spidering

* `robots.txt`
  * directive d'indexation par user agent
    * Disallow: dossier, pages non indexées
    * valable pour les crawlers sympas et polis

<pre><code data-stream class="hljs bash">wget -r -e robots=off -l 0 http://targ.et</code></pre>

<pre><code data-stream class="hljs bash">nmap --script=http-robots.txt  www.lemonde.fr</code></pre>

#### spidering VS guessing

Note:
- arbo non exportable
- sitemap.xml
- zap pas très bon pour suivre les urls
    - javascripts burp est meilleur
    - burp / zap en mode proxy empile les urls visitées
        - pas frocément devinable par le programme
    - gère les urls qui peuvent entraver le spidering (/logout)
- balise META permettant le même type de chose quie le robots.txt
    - sur le cache ou l'indexation
- pas discret
- httracks
- là où il y a login / pass
    - comptes par défaut
    - mot de passe triviaux
    - brute force


## [Nikto](https://cirt.net/Nikto2)

* Recherche de point d'entrées classiques
  * grosse bases de données
  * teste les points d'entrée des CMS classiques
  * phpinfo.php
  * server-satuts
  * conf apache
  * fingerprints de l'OS
  * robots.txt
* User-agent personnalisable

<pre><code data-stream class="hljs bash" style="font-size: 25px">nikto --host go.od -Format htm --output /tmp/go.od.html</code></pre>

Note:
- trouve le phpmyadmin


## Fuzzing

* Fuzzing
  * n'importe quoi
    * valeurs incorrectes
    * non ascii
    * binaire
  * random ou pseudo random
  * valeurs malines
    * fermeture de caractère


## Fuzzing

* On cherche
  * des requêtes avec des tailles différentes
    * messages d'erreur
    * paramètres HTTP repris dans la page
  * des codes http inattendus
    * tout ce qui se différencie du cas normal

#### [<i class="fa fa-github"></i> fuzzdb](https://github.com/fuzzdb-project/fuzzdb) dictionnaire de fuzzing

Note:
- mode comparaison de burb
- fuzzdb réinjectable dans burp intruder
- spidering VS guessing


## [DirBuster](https://www.owasp.org/index.php/Category:OWASP_DirBuster_Project)

* Produit [OWASP](https://www.owasp.org)
* Copie en locale
* Peut spécifier
    * une liste de pattern
    * du brute force pure
        * itératif
    * des extensions de fichiers (bak, tmp, new, ...)

<small>[Hack Like a Pro: How to Find Directories in Websites Using DirBuster](http://null-byte.wonderhowto.com/how-to/hack-like-pro-find-directories-websites-using-dirbuster-0157593/)</small>


## [<i class="fa fa-github"></i> patator](https://github.com/lanjelot/patator)

* [Produit HSC](http://www.hsc.fr/ressources/outils/patator/download/README)
* Brute force en tout genre
* Fuzzing http
  * on peut tester tous les paths de FILE
  * avec tous les rep de FILE0
  * tous les noms de fichiers de FILE1
  * toutes les extensions de fichiers FILE2    

<pre><code data-stream class="hljs bash" style="font-size: 14px">patator.py http_fuzz url=http://client/FILE0/FILE1FILE2 0=paths.txt 1=files.txt 2=exts.txt --ignore=404</code></pre>

<small>[<i class="fa fa-film"></i> Patator -- Brute-Force Attack And Dns Reverse And Forward Lookup](http://www.securitytube.net/video/7060)</small>

Note:
- utilisation dans burp possible aussi


## <i class="fa fa-medkit"></i> Se protéger

* Bien configurer son serveur
  * [Apache Tips & Tricks: Disable Directory Indexes](http://www.ducea.com/2006/06/26/apache-tips-tricks-disable-directory-indexes/)
  * [Per-user web directories](https://httpd.apache.org/docs/2.2/howto/public_html.html)
  * [<strike>mod_status</strike>](http://httpd.apache.org/docs/2.2/mod/mod_status.html)


## <i class="fa fa-medkit"></i> Se protéger

* Limiter / faker / filtrer les bannières / headers
  * [Changing Apache Server Name To Whatever You Want With mod_security On Debian 6](https://www.howtoforge.com/changing-apache-server-name-to-whatever-you-want-with-mod_security-on-debian-6)
  * [Apache Tips & Tricks: Hide PHP Version (X-Powered-By)](http://www.ducea.com/2006/06/16/apache-tips-tricks-hide-php-version-x-powered-by/)
  * [A practical approach for defeating Nmap OS-Fingerprinting](https://nmap.org/misc/defeat-nmap-osdetect.html)
  * les autres services


## <i class="fa fa-medkit"></i> Se protéger

* Eviter les messages d'erreurs trop bavard en prod
  * `php.ini`
    * [display_errors](http://php.net/manual/fr/errorfunc.configuration.php#ini.display-errors) à Off
      * fait taire mysql
      * le @ peut "muter" une fontcion
    * [directive log_errors](http://php.net/manual/fr/errorfunc.configuration.php#ini.log-errors) à On
    * [mysql.trace_mode](http://php.net/manual/fr/mysql.configuration.php#ini.mysql.trace-mode) à Off
    * [error_reporting = 0](http://php.net/manual/fr/function.error-reporting.php#example-504)


## <i class="fa fa-medkit"></i> Se protéger

* Utiliser un système de déploiement automatisé
  * utilisation d'un CVS
  * utilisation des environnements de dev / test / prod

* Privilégier les commentaires côté serveur
  * éliminer les commentaires HTML / CSS / JS au déploiement


## <i class="fa fa-medkit"></i> Se protéger

* Proscrire les services "en clair"
  * ftp
  * pop3
  * imap
  * rlogin
  * telnet ...


## <i class="fa fa-medkit"></i> Se protéger

* Mettre à jour régulièrement
  * OS
  * services
  * librairies
  * CMS
