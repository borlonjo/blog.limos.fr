# Anticiper

## aka le métier qui rentre ...


## Trouver des mentors

![Bruce Schneier quote](images/anticiper/Bruce-Schneier-Quotes-1.jpg "Bruce Schneier quote")


## Trouver des mentors

![Bruce Schneier quote](images/anticiper/Bruce-Schneier-Quotes-5.jpg "Bruce Schneier quote")

<i class="fa fa-quote-left"></i> la sécurité n'est pas un produit c'est un process <i class="fa fa-quote-right"></i> _Bruce Schneier_


## Organiser sa veille

* [CVE (Common Vulnerabilities and Exposures)](http://cve.mitre.org/index.html)
  * MITRE est une société qui
    * a lancé le projet CVE ("Common Vulnerabilities and Exposures")
      * facilite l'échange d'information de sécurité
    * maintient les ID de vulnérabilité
* [CVE Details](https://www.cvedetails.com/)
  * custom RSS


## phpMyAdmin

![phpMyAdmin](images/anticiper/phpmyadmin-vuln.png "phpMyAdmin")<!-- .element width="90%" -->

Note:
- bien percé
  - Ne pas trop exposer
    - bien pratique
      - Utiliser un client lourd
        - MySQL Workbench


## Organiser sa veille

  * [National Vulnerability Database - US](https://nvd.nist.gov/)
  * [CERT EU](https://cert.europa.eu/cert/filteredition/en/CERT-LatestNews.html)  
  * [CERT FR](http://www.cert.ssi.gouv.fr/cert-fr/certfr.html) - [archive 2015](http://www.cert.ssi.gouv.fr/site/2015index.html)
  * [CERT RENATER](https://services.renater.fr/ssi/cert/index) - [sécurité](https://services.renater.fr/ssi/securite/index)
  * et d'autres [CSIRT](http://www.cert.ssi.gouv.fr/cert-fr/cert.html)


## Organiser sa veille

* Sources reconnues
  * [Bugtraq - Security Focus](http://www.securityfocus.com/bid)
  * [Open Source Software Security Wiki](http://oss-security.openwall.org/wiki/)
    * [<i class="fa fa-envelope"></i> Mailing list](http://oss-security.openwall.org/wiki/mailing-lists/oss-security)
    * [<i class="fa fa-twitter"></i> openwall](https://twitter.com/openwall)
  * [SecLists.Org Security Mailing List Archive](http://seclists.org/)


## Organiser sa veille

* Sources alternatives
  * [Exploit Databse](https://www.exploit-db.com)
  * [Shodan - Popular Searches](https://www.shodan.io/explore/popular)
  * [Shodan - Recently Shared](https://www.shodan.io/explore/recent)

* Sources spécialisés
  * [WPScan Vulnerability Database](https://wpvulndb.com/)
  * [drupalexploit.com](http://www.drupalexploit.com/)


## Organiser sa veille

* <i class="fa fa-twitter"></i> twitter
  * [<i class="fa fa-list-alt"></i> ma liste ssi-infosec-hack](https://twitter.com/mazenovi/lists/ssi-infosec-hack)
* <i class="fa fa-wordpress"></i> blog
  * en anglais
    * https://googleonlinesecurity.blogspot.fr/
    * https://www.schneier.com/
    * https://nakedsecurity.sophos.com/

Note:
- parler de netvibes et yahoo pipes (qui ets mort)
  - considérer les solutions hébergées


## Organiser sa veille

* <i class="fa fa-wordpress"></i> blog
  * en français
    * http://www.bortzmeyer.org/
    * https://reflets.info/    
    * http://zythom.blogspot.fr/
    * http://www.nolimitsecu.fr
    * http://news0ft.blogspot.fr


## Organiser sa veille

* autre ...
  * en français
    * http://www.security-feeds.com/
    * http://vigilance.fr/
    * http://www.datasecuritybreach.fr/
    * http://www.zataz.com/
    * http://www.globalsecuritymag.fr/
    * http://assiste.com/


## OWASP

* depuis janvier 2001
* fondation Américaine
    * [à but non lucratif](http://en.wikipedia.org/wiki/501%28c%29_organization#501.28c.29.283.29)
* en France
    * Association loi 1901
    * participe au [clusif - Club de la Sécurité de l’Information Français](http://www.clusif.asso.fr/)
    * Ludovic Petit & [Sebastien Gioria ](https://twitter.com/spoint)
      * [L'OWASP, l'univers, et le reste](https://air.mozilla.org/talks-owasp-afup-firefoxos-security-mozilla-firefoxos-what-is-owasp-by-sebastien-gioria/)


## OWASP - Indépendance

* composé d'expert indépendants
* indépendant des fournisseurs de solution
* indépendant des gouvernements
* Les projets sont opensource
* nombreux adhérents
  * entreprises
  * institutions
  * [individus](https://docs.google.com/spreadsheets/d/1FQEj2xQb1uTxZMXshPs0suy1Bkb5iYCbHH_vrzHMVa4/edit)
  * [join now](http://myowasp.force.com/memberappregion): <strike>adhésion gratuite</strike> donation obligatoire 50$ minimum

Note:
- le marché de la sécurité est blindé de commericaux
  - et de conflit d'intérêt


## OWASP - Projets

!["OWASP Flagship mature project"](images/anticiper/Flagship_banner.jpg "OWASP Flagship mature project")<!-- .element width="70%"-->
!["OWASP Lab medium level project"](images/anticiper/Lab_banner.jpg "OWASP Lab medium level project")<!-- .element width="70%"-->
!["OWASP Low activity project"](images/anticiper/Low_activity.jpg "OWASP Low activity project")<!-- .element width="70%"-->
!["OWASP Incubator new projects"](images/anticiper/Incubator_banner.jpg "OWASP Incubator new projects")<!-- .element width="70%"-->


## [Owasp projects](https://www.owasp.org/index.php/Category:OWASP_Project#tab=Project_Inventory)

* [OWASP Top Ten project](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project)
* [OWASP Testing Guide](https://www.owasp.org/index.php/OWASP_Testing_Project)
* [OWASP Risk Rating Methodology](https://www.owasp.org/index.php/OWASP_Risk_Rating_Methodology)
* [OWASP Code Review Guide](https://www.owasp.org/index.php/Category:OWASP_Code_Review_Project)
* [OWASP Developer Guide](https://www.owasp.org/index.php/OWASP_Guide_Project)


## [Owasp projects](https://www.owasp.org/index.php/Category:OWASP_Project#tab=Project_Inventory)

* [OWASP Enterprise Security API](https://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API)
* [OWASP Application Security Desk Reference](https://www.owasp.org/index.php/Category:OWASP_ASDR_Project)
* [OWASP Cheat Sheets](https://www.owasp.org/index.php/Cheat_Sheets)
* [OWASP Application Security Verification Standard (ASVS)](http://www.owasp.org/index.php/ASVS)
* [OWASP Legal Project](http://www.owasp.org/index.php/Category:OWASP_Legal_Project)
* [and more ...](https://www.owasp.org/index.php/Category:OWASP_Project#tab=Project_Inventory)


## [ANSSI](href="http://www.ssi.gouv.fr)

* organisme de
  * [formation](http://www.ssi.gouv.fr/fr/anssi/formations/)
    * sensibilisation / recommandation
      * [guide d'hygiène informatique](http://www.ssi.gouv.fr/IMG/pdf/guide_hygiene_informatique_anssi.pdf)
      * [Objectifs de sécurité (dans le cadre du plan vigipirate)](http://www.ssi.gouv.fr/fr/defense-des-si/les-plans-gouvernementaux/) et au niveau [OIV](http://www.ssi.gouv.fr/actualite/lanssi-sattele-aux-decrets-dapplication-de-la-lpm-portant-sur-la-protection-des-operateurs-dimportance-vitale/)
      * [cybersécurité des système industriels](http://www.ssi.gouv.fr/fr/guides-et-bonnes-pratiques/recommandations-et-guides/securite-des-systemes-industriels/la-cybersecurite-des-systemes-industriels.html)
      * [Sécuriser un site web](http://www.ssi.gouv.fr/guide/recommandations-pour-la-securisation-des-sites-web/)
      * [Protéger son site Internet des cyberattaques](http://www.ssi.gouv.fr/actualite/proteger-son-site-internet-des-cyberattaques/)


## [ANSSI](href="http://www.ssi.gouv.fr)

* certfication
  * [de produits de sécurité](http://www.ssi.gouv.fr/fr/produits-et-prestataires/produits-certifies-cc/)
  * [de prestataires de services de confiance](http://www.ssi.gouv.fr/fr/produits-et-prestataires/prestataires-de-services-de-confiance-qualifies/)
* d'intervention / investigation

[<i class="fa fa-book"></i> La Stratégie nationale pour la sécurité du numérique : une réponse aux nouveaux enjeux des usages numériques](http://www.ssi.gouv.fr/actualite/la-strategie-nationale-pour-la-securite-du-numerique-une-reponse-aux-nouveaux-enjeux-des-usages-numeriques/)

Note:
- de la com pourrie aussi [HACK ACADEMY : DE DRÔLES DE HACKERS AU SERVICE DE LA PRÉVENTION](http://www.ssi.gouv.fr/actualite/hack-academy-de-droles-de-hackers-au-service-de-la-prevention/)


## Autres entités SSI françaises

* [OzSSI](http://www.ssi.gouv.fr/agence/cybersecurite/ozssi/)
  * [Zomm sur les OzSSI](http://www.ssi.gouv.fr/actualite/zoom-sur-les-ozssi/)
* [OSSIR](http://www.ossir.org/)
* [CLUSIF](https://www.clusif.asso.fr/)
  * [CLUSIR Rhône Alpes](http://www.clusir-rha.fr/)
  * CLUSIR Auvergne?


## Best Practices / Cheat sheets / Checklists

* Généraliste
  * http://www.sans.org/critical-security-controls/
  * [The Joel Test: 12 Steps to Better Code](http://www.joelonsoftware.com/articles/fog0000000043.html)
* OWASP
  * [OWASP Proactive Controls](https://www.owasp.org/index.php/OWASP_Proactive_Controls)
* Linux
  * https://www.sans.org/media/score/checklists/linuxchecklist.pdf


## Best Practices / Cheat sheets / Checklists

* Apache
  * http://httpd.apache.org/docs/2.4/misc/security_tips.html
  * http://geekflare.com/10-best-practices-to-secure-and-harden-your-apache-web-server/
  * http://www.tecmint.com/apache-security-tips/
  * http://blogs.reliablepenguin.com/2012/10/09/apache-configuration-best-practicies


## Best Practices / Cheat sheets / Checklists

* Nginx
  * http://www.cyberciti.biz/tips/linux-unix-bsd-nginx-webserver-security.html
* Wordpress
  * http://mazenovi.github.io/blog/2015/01/28/securing-wordpress/
* Drupal
  * https://www.drupal.org/project/security_review


#### Proposition d'Hervé Schauer

[HSC Newsletter](https://www.hsc.com.vn/en/help-center/other-services/hsc-newsletter)  --  N°135

* Utilisez-vous systématiquement des requêtes SQL préparées (connues aussi sous le nom de requêtes paramétrées) ?
* Utilisez-vous la gestion de session d'un cadriciel/bibliothèque fiable ?
* Encodez-vous systématiquement les valeurs venant de l'extérieur (utilisateur, base de données...) avant de les envoyer vers l'affichage ?
* Utilisez-vous SSL/TLS sur la totalité de votre site ?


#### Proposition d'Hervé Schauer

* Stockez-vous et vérifiez-vous les mots de passe utilisateurs uniquement en les dérivant avec *sha256crypt*, *bcrypt*, *scrypt*, *pbkdf2(sha256)* ou *argon2*?
* Évitez-vous systématiquement d'utiliser les entrées utilisateurs pour des appels systèmes ?
* Stockez-vous les téléchargements utilisateurs vers l'application (uploads) dans un environnement restreint ?


#### Proposition d'Hervé Schauer

* Séparez-vous les paramètres secrets du code ?
* Faites-vous un suivi de version de tout votre code ?
* Gérez-vous vos dépendances et leur sécurité ?

Note:
- c'est le web beaucoup de gens ne savent pas ce qu'ils racontent ...
  - faire marcher la tête et ne pas totu copier / coller sur un espace de prod
- pensez la sécurité au moment du choix du design pattern


## utiliser tls

* ne jamais utiliser FTP
  * ftps et sftp fonctionnent parfaitement
  * rsync et scp aussi
    * chrooter un user ssh
      * https://stdout.cowthink.org/setup-chroot-jails-in-linux-and-only-allow-sftp/
* généraliser l'utilisation d'https
  * [Google favorise les sites en HTTPS, c'est un critère officiel du référencement naturel](http://www.webrankinfo.com/dossiers/conseils/https-critere-seo)
* désactiver les services de types POP3, IMAP, SMTP
  * utiliser les versions SSLifiées


#### [ISPConfig](http://www.ispconfig.org/page/home.html)

![ISPConfig](images/anticiper/ispconfig.jpg "ISPConfig")

[tuto d'install](https://www.howtoforge.com/tutorials/ispconfig/)

Note:
- suExec, suPHP, chrooting avec jailkit, rkhunter
- [auto install](https://www.howtoforge.com/tutorial/ispconfig-install-script-debian/)


## Symfony

* A1  
  * ORM (Doctrine / Propel)
    * SQLi
  * librairie system / Service Parameters typés
    * Command injection
  * namespace / service
    * LFI / RFI


## Symfony

* A2  
  * FOSUserBundle
* A3
  * twig
    * échappe par défaut
* A4  
  * security.yml
    * Firewall donnant une vue globale sur la sécurité des objets


## Symfony

* A5
  * configuration par défaut secure
* A6
  * [igorw/IgorwFileServeBundle](https://github.com/igorw/IgorwFileServeBundle)
* A7
  * système d'ACL


## Symfony

* A8
  * système de jeton par défaut
    * pour totue soumission de formulaire
* A9
  * basé sur github & composer
* A10
  * Service Parameters typés


## Parler sécurité

* Revue de code
* Rendre les checklists collaboratives
* Bug bounty
  * <i class="fa fa-beer"></i> Bières, restos, sandwichs
  * ce que font les grands
    * google, FB, etc ...
* DevOps
  * Be fullstack
* [Discuter les modèles (exposer ou cacher)](https://en.wikipedia.org/wiki/Open-source_software_security)

Note:
- si Microsoft ouvre tout ce n'est pas un hasard c'est que ca marche


#### Positiver

![Be positive](images/anticiper/bepositive.jpg "Be positive")<!-- .element width="50%" -->

Note:
- effort supplémentairer à moyen constant
  - pas le choix sur la présentation


## Utiliser des outils de gestion de dépendance

* Bonne pratique
  * permet d'éviter des problèmes de compatibilités
  * permet une mise à jour globale
  * permet un meilleur déploiement
* [composer](https://getcomposer.org/) (PHP)
* [bower](http://bower.io/) (Javascript client)
* [npm](https://www.npmjs.com/) (Javascript server)
* [pip](https://pypi.python.org/pypi/pip) (Python)
* [gems](https://rubygems.org/) (Ruby)


## Analyser le code

* pas d'outil miracle
  * peut être dans les très spécialisés et les très chers?
* statique
  * lecture du code
    * grep -nR
    * [netbeans](https://netbeans.org/downloads/)
      * ctrl+clic
    * [OWASP Code Review Project](https://www.owasp.org/index.php/Category:OWASP_Code_Review_Project)


## Analyser le code

* dynamique
  * exécution du code
    * [OWASP Testing Project](https://www.owasp.org/index.php/OWASP_Testing_Project)
      * ~ Fuzzing
    * [XDebug](http://xdebug.org/)
    * [KCacheGrinder](http://kcachegrind.sourceforge.net/html/Home.html)
    * [BlackFiler Profiler](https://blackfire.io/)

Note:
- les outils de gestion de version rendent services
- long et souvent très spécialisé


## Tests & Intégration continue (CI)

* utiliser un outil de versionning
  * [git](https://git-scm.com/), [svn](https://subversion.apache.org/), [mercurial](https://www.mercurial-scm.org/wiki/)
  * rétrochronologie des vulnérabilités
* utiliser les environnements
  * prod, dev, test, staging, deploy, ...
* les secrets de la CI
* [vault by HashiCorp](https://www.vaultproject.io/)

évite le leak de configuration


## Tests & Intégration continue (CI)

* tester
  * intégrer la sécurité au tests
    * aspect fonctionnel
      * [behat](http://docs.behat.org/en/v2.5/)
      * [casperjs](http://casperjs.org/) sur la base de [phantomjs](http://phantomjs.org/)
      * [selenium](http://www.seleniumhq.org/)
* jouer les tests régulièrement via la ci [gitlab](https://gitlab.com/), [jenkins](https://jenkins-ci.org/), [Travis](https://travis-ci.org/), ...


## Déploiement logiciel

* via la ci [gitlab](https://gitlab.com/), [jenkins](https://jenkins-ci.org/), [Travis](https://travis-ci.org/), ...
* [githook](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)
* [capistrano](http://capistranorb.com/)
  * [capifony](http://capifony.org/)
    * [<i class="fa fa-github"></i> capistrano/symfony](https://github.com/capistrano/symfony)
* [Fabric](http://www.fabfile.org/)
* bash <3


## filtrer

* [ngnix](http://nginx.org/)
  * permet de filtrer rapidement et efficacement
* [Web application Firewal (WAF)](http://www.cert-ist.com/pub/files/Document_Cert-IST_000333.pdf)
  * [mod_security](https://www.modsecurity.org/)
    * permet entre autre de logger la méthode post facilement
      * gare à l'espace disque
    * [<i class="fa fa-github"></i> SpiderLabs/owasp-modsecurity-crs](https://github.com/SpiderLabs/owasp-modsecurity-crs)
      * expressions régulières complexes


## Monitorer

* Avoir un outil d'analyse de log
  * permet de connaître le bruit
  * beaucoup d'attaques a priori


## Etudier des pistes radicales

* déporter le back office
* utiliser la génération de code
  * [<i class="fa fa-github"></i> sculpin/sculpin](https://sculpin.io)
  * [<i class="fa fa-github"></i> getpelican/pelican](https://github.com/getpelican/pelican)
* interdire l'upload de fichier
* utiliser markdown (pure) pour la mise en forme
  * ou tout autre système de texte enrichi
* ne rien mettre sur le web


## S'entrainer

* jamais sur de la prod
  * ni la vôtre
  * ni celle des concurrents
* Challenges
  * http://www.root-me.org/
  * https://www.newbiecontest.org/
  * https://github.com/ctfs


## S'entrainer

* VM
  * http://www.bonsai-sec.com/en/research/moth.php
  * http://sourceforge.net/projects/lampsecurity/
  * http://hackxor.sourceforge.net/cgi-bin/index.pl
  * http://sourceforge.net/projects/exploitcoilvuln/
  * https://www.vulnhub.com/entry/metasploitable-1,28/
  * http://sourceforge.net/projects/metasploitable/


## S'entrainer

* Weak apps
  * http://sourceforge.net/projects/mutillidae/
  * http://sechow.com/bricks/
  * https://www.pentesterlab.com/
  * http://hackingdojo.com/dojo-media/


## Conclusion

<div style="text-align: center">
  ![Conseil de Jedi](images/anticiper/yoda.gif "Conseil de Jedi")
</div>


## Conclusion

* <i class="fa fa-bullhorn"></i> une veille efficace tu maintiendras
* <i class="fa fa-bullhorn"></i> toutes les mises à jour asap tu effectueras
* <i class="fa fa-bullhorn"></i> toutes tes entrées tu filteras
* <i class="fa fa-bullhorn"></i> toutes tes sorties tu échapperas
* <i class="fa fa-bullhorn"></i> par listes blanches tu réfléchiras
* <i class="fa fa-bullhorn"></i> les bonnes pratiques tu étudieras
* <i class="fa fa-bullhorn"></i> un système fiable de déploiement tu utiliseras
* <i class="fa fa-bullhorn"></i> avec ton équipe de sécurité tu parleras

Note:
- cacher n'est pas protégé
