# CSRF

## aka Cross-Site Request Forgery


### <i class="fa fa-cogs"></i> Principe

* affecte toute ressource disponible directement
  * sans étape intermédiaire
    * avec authentification
      * usurpation de session
    * ou pas
      * redirection arbitraire


### <i class="fa fa-cogs"></i> Principe

* le but est de rediriger un utilisateur vers une url

  * à l'insu de son plein gré

* pour qu'il clique tous les coups sont permis


### <i class="fa fa-user-secret"></i> Que peut on faire?

* exécuter des opérations avec les permissions d'un autre utilisateur
* clickjacking
* spam

Note:
- l'action avec privilège
  - urls connues (CMS, ...)
    - on espère que l'utilisateur est logué
- spam de commentaire
  - urls connues (CMS, ...)


## Mode opératoire

<div style="text-align: center">
  ![alt text](images/csrf/csrf.png "CSRF")
</div>

Note:
- rappel ici mail, mais aussi
  - XSS
  - lien déguisé
  - url shortner


## Différentes techniques

* Url ou formulaires forgés relayés via XSS
* redirection arbitraire via XSS

* Social engineering
  * initulé de lien malicieux
  * url shortner
  * le lien image déguisé

* Phishing
  * par mail par exemple
    * approche probabiliste


## Différentes techniques

* Modficiation de l'overlay en css
  * likejacking ou +1jacking
    * [Arrêtez de vous faire avoir sur Facebook !!! ](http://www.mycommunitymanager.fr/arretez-de-vous-faire-avoir-sur-facebook/)

```css
a#malice {
  display: block;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  opacity: 0.001;
}
```

Note:
- XSS réfléchi ou stocké ca marche
  - ya toujours un con qui clique
- insolvable click jacking insolvable
  - aucun moyen de distinguer les like légitimes
    - ils proviennet tous d'un serveur externe à facebook
      - possibilité de blacklist pour des sites réputés frauduleux


### [CSRF stored - security low](http://dv.wa/vulnerabilities/csrf/)  

url vulnérable

```http
http://dv.wa/vulnerabilities/csrf/?password_new=password&password_conf=password&Change=Change
```

passage de paramètres via GET en injectant

```http
<iframe
  src="javascript:window.location='http://dv.wa/vulnerabilities/csrf/?password_new=1337&password_conf=1337&Change=Change';"
  height="0" width="0" style="border:0;">
</iframe>
```

via POST avec en scriptant

```http
<form action="http://dv.wa/vulnerabilities/csrf/admin.php" method="post" id="formid"
  onload="document.getElementById('formid').submit();">
  <input type="hidden" name="password_new" value="1337"/>
  <input type="hidden" name="password_conf" value="1337"/>
</form>
```

Note:
- analyser la requête de changement de mot de passe avec burp
- forger une url
- forger un form
  - injecter via XSS stored par exemple
- problème de l'action silencieuse et aveugle
  - on ne sait pas sur qui ca marche
  - si on a une liste des utilisateurs du site le brute force est un option


## <i class="fa fa-medkit"></i> Se préserver

* **Fausses Bonnes Idées**
  * utiliser la méthode POST
  * vérifier le referer (ou n'importe quelle autre en-tête)
    * [CSRF stored - security medium](http://dv.wa/vulnerabilities/csrf/)  
* Double Submit Pattern
  * [CSRF stored - security high](http://dv.wa/vulnerabilities/csrf/)


## <i class="fa fa-medkit"></i> Synchronizer Token Pattern     

<div style="text-align:center">
  ![alt text](images/csrf/csrf-stp.png "CSRF")
</div>


## <i class="fa fa-medkit"></i> Synchronizer Token Pattern     

* token de session
  * jeton dans un formulaire OU dans l'url ET dans la session utilisateur
    * avec éventuellement une salaison propre à l'utilisateur comme l'IP par exemple
      * on préserve ainsi le token et on cache un manque d'entropie éventuel

```http
<input type="hidden" name="csrftoken" value="KbyUmhTLMpYj7CD2di7JKP1P3qmLlkPt">
```


## <i class="fa fa-medkit"></i> Synchronizer Token Pattern     

* <i class="fa fa-frown-o"></i> compliqué avec une utilisation massive de XMLHttpRequest
* <i class="fa fa-frown-o"></i> possibilité de [jetons malins via fixation de session via le referer](http://voices.washingtonpost.com/securityfix/2009/07/weaponizing_web_20.html) ou de brute force
  * utiliser des nonces pourrait être une solution


## <i class="fa fa-medkit"></i> [Cookie-To-Header Token](https://en.wikipedia.org/wiki/Cross-site_request_forgery#Cookie-to-Header_Token)

* adapté à une utilisation massive de JS
* basé sur la [Same Origin Policy](https://en.wikipedia.org/wiki/Same-origin_policy)

* A l'authentification le serveur envoie un cookie contenant un jeton aléatoire valable pendant toute la session de l'utilisateur

```js
Set-Cookie: Csrf-token=i8XNjC4b8KVok4uw5RftR38Wgp2BFwql; expires=Thu, 23-Jul-2015 10:25:33 GMT; Max-Age=31449600; Path=/
```


## <i class="fa fa-medkit"></i> [Cookie-To-Header Token](https://en.wikipedia.org/wiki/Cross-site_request_forgery#Cookie-to-Header_Token)

* JavaScript lit le jeton et le renvoie dans un header HTTP spécifique à chaque requête

```js
X-Csrf-Token: i8XNjC4b8KVok4uw5RftR38Wgp2BFwql
```

* Le serveur vérifie la validité du token


## <i class="fa fa-medkit"></i> [reCAPTCHA - Google](https://www.google.com/recaptcha/intro/index.html)

[http://dvwa.com/vulnerabilities/captcha](http://dvwa.com/vulnerabilities/captcha)

* Security level: low
  * action configurée en 2 étapes
    * Etape 1
      * validation du captcha
    * Etape 2
      * exécution de l'action
  * en modifiant le paramètre step à 2 on bypass le captcha


## <i class="fa fa-medkit"></i> [reCAPTCHA - Google](https://www.google.com/recaptcha/intro/index.html)

* Security level: medium
  * action configurée en 3 étapes
    * Etape 1
      * validation du captcha
    * Etape 2
      * Confirmation de l'action
    * Etape 3
      * exécution de l'action
  * en modifiant le paramètre step à 2 et en ajoutant Change=Change on bypass le captcha


## <i class="fa fa-medkit"></i> Se préserver

* Côté client
  * [RequestPolicy](https://addons.mozilla.org/fr/firefox/addon/requestpolicy) protège mais peut empêcher certains sites de fonctionner
  * [CsFire](https://addons.mozilla.org/fr/firefox/addon/csfire) protège un peu en enlevant toute information d'authentification pour les requêtes cross-site
  * [NoScript](https://addons.mozilla.org/fr/firefox/addon/noscript) va prémunir des scripts en provenance de de sites non sûrs
  * [Self-Destructing Cookies](https://addons.mozilla.org/fr/firefox/addon/self-destructing-cookies) permet de réduire la fenêtre d'attaque en supprimant les cookies dès qu'ils ne sont plus associé à un onglet actif
