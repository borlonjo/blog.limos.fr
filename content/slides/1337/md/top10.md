## [Owasp top 10 (2013)](https://www.owasp.org/index.php/Top_10_2013-Top_10)

#### <i class="fa fa-file-pdf-o"></i> [FR](http://owasptop10.googlecode.com/files/OWASP%20Top%2010%20-%202013%20-%20French.pdf)

### <i class="fa fa-book"></i> [All Attack types](https://www.owasp.org/index.php/Category:Attack)


## [Owasp top 10 (2013)](https://www.owasp.org/index.php/Top_10_2013-Top_10)

1. [Injection](https://www.owasp.org/index.php/Top_10_2013-A1-Injection)
2. [Broken Authentication and Session Management](https://www.owasp.org/index.php/Top_10_2013-A2-Broken_Authentication_and_Session_Management)
3. <a href="https://www.owasp.org/index.php/Top_10_2013-A3-Cross-Site_Scripting_(XSS)">Cross-Site Scripting (XSS)</a>
4. [Insecure Direct Object References](https://www.owasp.org/index.php/Top_10_2013-A4-Insecure_Direct_Object_References)
5. [Security Misconfiguration](https://www.owasp.org/index.php/Top_10_2013-A5-Security_Misconfiguration)
6. [Sensitive Data Exposure](https://www.owasp.org/index.php/Top_10_2013-A6-Sensitive_Data_Exposure)
7. [Missing Function Level Access Control](https://www.owasp.org/index.php/Top_10_2013-A7-Missing_Function_Level_Access_Control)
8. <a href="https://www.owasp.org/index.php/Top_10_2013-A8-Cross-Site_Request_Forgery_(CSRF)">Cross-Site Request Forgery (CSRF)</a>
9. [Using Components with Known Vulnerabilities](https://www.owasp.org/index.php/Top_10_2013-A9-Using_Components_with_Known_Vulnerabilities)
10. [Unvalidated Redirects and Forwards](https://www.owasp.org/index.php/Top_10_2013-A10-Unvalidated_Redirects_and_Forwards)


## [Owasp top 10 (2017)](https://www.owasp.org/index.php/Top_10-2017_Top_10)

1. [<i class="fa fa-hand-o-right" aria-hidden="true"></i> Injection](https://www.owasp.org/index.php/Top_10-2017_A1-Injection)
2. [<i class="fa fa-hand-o-right" aria-hidden="true"></i> Broken Authentication and Session Management](https://www.owasp.org/index.php/Top_10-2017_A2-Broken_Authentication)
3. [<i class="fa fa-hand-o-up" aria-hidden="true"></i> Sensitive Data Exposure](https://www.owasp.org/index.php/Top_10-2017_A3-Sensitive_Data_Exposure)
4. <a href="https://www.owasp.org/index.php/Top_10-2017_A4-XML_External_Entities_(XXE)"><i class="fa fa-hand-peace-o" aria-hidden="true"></i> XML External Entities (XXE)</a>
5. [<i class="fa fa-hand-peace-o" aria-hidden="true"></i> Broken Access Control](https://www.owasp.org/index.php/Top_10-2017_A5-Broken_Access_Control)
6. [<i class="fa fa-hand-o-down" aria-hidden="true"></i> Security Misconfiguration](https://www.owasp.org/index.php/Top_10-2017_A6-Security_Misconfiguration)
7. <a href="https://www.owasp.org/index.php/Top_10-2017_A7-Cross-Site_Scripting_(XSS)"><i class="fa fa-hand-o-down" aria-hidden="true"></i> Cross-Site Scripting (XSS)</a>
8. [<i class="fa fa-hand-peace-o" aria-hidden="true"></i> Insecure Deserialization](https://www.owasp.org/index.php/Top_10-2017_A8-Insecure_Deserialization)
9. [<i class="fa fa-hand-o-right" aria-hidden="true"></i> Using Components with Known Vulnerabilities](https://www.owasp.org/index.php/Top_10-2017_A9-Using_Components_with_Known_Vulnerabilities)
10. [<i class="fa fa-hand-peace-o" aria-hidden="true"></i> Insufficient Logging&Monitoring](https://www.owasp.org/index.php/Top_10-2017_A10-Insufficient_Logging%26Monitoring)


## Répartition par Attaques

![Relative Portions of Each Attack Type](images/top10/Relative_Portions_of_Each_Attack_Type.png "Relative Portions of Each Attack Type")

[source IMPERVA](http://www.imperva.com/docs/HII_Web_Application_Attack_Report_Ed4.pdf)


## Répartition par Attaques

* Cross-site scripting (XSS)
* SQL injection (SQLi)
* Remote File Inclusion (RFI)
* Local File Inclusion (LFI)
* Email extraction (EmExt)
* Directory traversal (DT)
* Comment Spamming (ComSpm)
<!-- https://www.whitehatsec.com/resource/stats.html -> owncloud/ssi/2015-Stats-Report.pdf to print -->
<!-- http://www.symantec.com/security_response/publications/threatreport.jsp -->
<!-- http://www.zdnet.fr/actualites/cybersecurite-a-quoi-s-attendre-dans-les-mois-qui-viennent-39822112.htm -->


## Autres top 10 ...

### <i class="fa fa-mobile"></i> [Top 10 - Mobile security](https://www.owasp.org/index.php/OWASP_Mobile_Security_Project#tab=Top_10_Mobile_Risks)

### <i class="fa fa-wordpress"></i> <i class="fa fa-drupal"></i>  [Top 10 des failles de sécurité des CMS](http://www.cms.fr/articles/525-top-10-des-failles-de-securite-des-cms.html)
