## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>

<div style="text-align: center;">
    <img src="/_/gdi/images/etes-vous-prets.jpg" width="70%"/>
</div>


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* être identifié comme CSSI
* organiser la sensibilisation
* anticiper l'arrivée de l'incident pour
  * dédramatiser
  * avoir une marche à suivre
    * éviter les erreurs techniques et organisationelles   
  * éviter la culpabilité
  * éviter la dissimulation
    * la déclaration d'incident est une obligation légale
    * rappeler les Chartes d'utilisation des moyens informatiques du CNRS et des partenaires

Note:
- vertu de l'identificaiton, facilite la remontée évite la dissimulation amène à un comportement intelligent vis à vis de l'incidetn
- sensibilisation bon point de départ


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* L’environnement organisationnel
  * Préparer ou se rapprocher des procédures de gestions de crise
  * Préparer ou se rapprocher des plans de communication 
    * Préparer la gestion des priorités avec la hiérarchie, pour  exemple :
      * P1 : les personnes
      * P2 : les informations « PPST » 
      * P3 : les informations personnelles
      * etc.
  * S'assurer que la hiérarchie est impliquée et consciente de sa responsabilité


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* L’environnement organisationnel
  * Préparer une fiche de contacts (hiérarchie, partenaires, tutelles, etc.) 
  * Identifier les chaines de sécurité (ISIRT ou CSIRT en local, régional, national, partenaires, etc.)    
  * Préparer des fiches réflexes
  * Connaitre les différents plan de continuité (PCA) et plan de reprise (PRA) du périmètre
    * par exemple, les bascules automatiques peuvent détruire les preuves
  * Disposer d’une liste d’outils pour l’acquisition (A2IMP)


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* L’environnement légal
* Etre à jour sur les déclarations CNIL
* Etre à jour sur la réglementation spécifique
* Avoir mis en place les mentions légales
   * Web: [http://www.cil.cnrs.fr/CIL/spip.php?page=mentions_legales](http://www.cil.cnrs.fr/CIL/spip.php?page=mentions_legales)
    * Systèmes: <img src="/_/gdi/images/notice_to_users.png" width="60%" />

Note: 
- Affaire bluetouf & kitetoa "il n'ya pas de délis sans intention de le connaître"


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* Gestion des traces
  * [Politique de gestion des traces d'utilisation des moyens informatiques et des services réseau au CNRS](https://aresu.dsi.cnrs.fr/IMG/pdf/Po_gest_traces.pdf)
    * Le CNRS a fait une déclaration à la CNIL relative aux logs
      * Conserver
        * Logs serveurs et postes de travail
        * Logs serveurs de messagerie
        * Logs serveurs Web
        * Logs Services réseaux + équipements d'extrémités (Firewalls, routeurs, …)
        * Logs de systèmes de détection d'intrusion (IDS)
        * Logs de applications spécifiques, dès lors qu'elles enregistrent des données de connexion, d'utilisation
  * La durée de conservation de ces journaux est fixée au maximum à 1 an (et au minimum à 1 an aussi ...)


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* Gestion des traces
  * Elle nous précise aussi la manière dont : 
    * on peut exploiter ces données (analyse, statistiques, …)
    * l'unité doit informer les utilisateurs de la gestion qui est faite des traces qui les concernent.
  * En fonction de ces informations, il apparaît qu’il est à minima fortement recommandé de :
    * Gérer ses logs sur tous ses serveurs
    * déployer une architecture de centralisation des logs
  * Solution préconisée par le CNRS:
    * rsyslog + outil d'analyse (logCheck, logwatch, logAnalyser, logStash, ElasticSearch, Kibana)


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* La gestion des traces
  * Les utilisateurs doivent être informés (CNIL)
    * Charte
  * Le traitement doit être proportionnel (CNIL)
    * Gestion pour supervision technique sans intrusion dans la vie privée
    * Analyse fine uniquement à la demande de la chaîne SSI / Défense CNRS si suspicion 
  * Destinataires   
    * Chaîne SSI / Défense CNRS
    * OPJ dans le cadre d’une enquête préliminaire ou commission rogatoire


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* Pour anticiper le recouvrement s'assurer
  * D’avoir un inventaire à jour des mots de passe et des séquestres
    * Physique
    * Numérique
    * Doit être frais et fonctionnel (à tester)
      * Etes vous en mesure de déverrouiller un périphérique chiffré pris au hasard sur votre parc?
  * D'avoir des sauvegardes fraîches et fonctionnelles (outil de sauvegarde / dashboard pour le suivi)   
  * Que vous n'êtes pas le seul à pouvoir réaliser tester ces opérations
    * A minima qu'elles soient documentées
    * idéalement former une équipe d'experts en gestion des incidents


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* Kit de survie GdI
  * liste des contacts
  * [bloc-note (journal)](/_/gdi/download/Fiche_intervention.pdf)
  * <i class="fa fa-gift"></i> disque dur externe (recueil de preuve) 
  * [Livecd A2IMP](https://mycore.core-cloud.net/public.php?service=files&t=1357ba7c8c2604a71f9695449fe6b39a) et doc [linux](https://mycore.core-cloud.net/public.php?service=files&t=f141f3741d356ac95bea7b92287111d7) et [windows](https://mycore.core-cloud.net/public.php?service=files&t=9d00808565b9c8fcd865940008e2b4ac)
    * [version antérieure](https://extra.core-cloud.net/collaborations/RSSI-CNRS/Documentation/Documents/ISO%20A2IMP.aspx)
  * Procédures (confinements, configurations, restaurations…)


## Préparer <img src="/_/images/PDCA/PDCA-P.png" width="20%" align="right"/>
* Faire de la veille de vulnérabilités SSI
  * [https://listes.services.cnrs.fr/wws/admin/corresp_ssi](https://listes.services.cnrs.fr/wws/admin/corresp_ssi)
    * Pour être abonné  à cette liste de diffusion, il faut faire partie d’au moins une de ces listes sous CORE
      * CSSI, CSSI Adjoints, ASR, RSSI Régionaux, RSSI Régionaux adjoints, RSSI Instituts, CRSSI ou être directement inscrit par le RSSI du CNRS.
    * [l'intranet du CNRS](https://extra.core-cloud.net/collaborations/RSSI-CNRS/Documentation/Documents/Forms/AllItems.aspx?TreeField=Folders)
      * [Que faire en cas de rançongiciel ?](https://extra.core-cloud.net/collaborations/RSSI-CNRS/Documentation/_layouts/WopiFrame.aspx?sourcedoc=/collaborations/RSSI-CNRS/Documentation/Documents/Rancongiciel.docx&action=default&Source=https%3A%2F%2Fextra.core-cloud.net%2Fcollaborations%2FRSSI-CNRS%2FDocumentation%2FDocuments%2FForms%2FAllItems.aspx%3FTreeField%3DFolders&DefaultItemOpen=1&DefaultItemOpen=1)
      * [Que faire en cas de défiguration de site web ?](https://extra.core-cloud.net/collaborations/RSSI-CNRS/Documentation/_layouts/WopiFrame.aspx?sourcedoc=/collaborations/RSSI-CNRS/Documentation/Documents/Traitement%20d%C3%A9figuration.docx&action=default&Source=https%3A%2F%2Fextra.core-cloud.net%2Fcollaborations%2FRSSI-CNRS%2FDocumentation%2FDocuments%2FForms%2FAllItems.aspx%3FTreeField%3DFolders&DefaultItemOpen=1&DefaultItemOpen=1)
  * [https://aresu.dsi.cnrs.fr/spip.php?rubrique73](https://aresu.dsi.cnrs.fr/spip.php?rubrique73)
  * [http://www.certa.ssi.gouv.fr/](http://www.certa.ssi.gouv.fr/)
  * [https://services.renater.fr/ssi/cert/info-secu](https://services.renater.fr/ssi/cert/info-secu)
  * [http://www.cert-ist.com/fra/ressources/Avis/Listedesderniersavis/](http://www.cert-ist.com/fra/ressources/Avis/Listedesderniersavis/)
  
<!-- * [http://vigilance.fr](http://vigilance.fr), [https://twitter.com/VUPEN](https://twitter.com/VUPEN), [https://secunia.com/community/advisories/historic/](https://secunia.com/community/advisories/historic/) -->