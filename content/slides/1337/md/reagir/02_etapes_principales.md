## &Eacute;tapes principales

<div style="text-align: center">
    <img src="/_/gdi/images/plan.jpg" width="70%" />
</div>


## Etapes de la gestion d’incidents
* La norme définit 5 étapes : [ISO27035 4.5](http://www.iso27001security.com/html/27035.html) (Information security incident management)
  * Prepare
  * Identify
  * Assess
  * Respond
  * Learn
* Cette formation traite la gestion d'incidents en 6 étapes
  * Préparer
  * Détecter, évaluer et réagir
  * Confiner et acquérir (rapidement)
  * Eradiquer et agir (efficacement)
  * Recouvrer (et prévenir)
  * Consolider


## Diagramme de flux

<div style="text-align: center;">
    <a href="/_/gdi/images/diagdeflux.png"><img src="/_/gdi/images/diagdeflux.png" width="40%" /></a>
</div>

Note:
- aspect amélioration


## Cartographie de la Gestion d'incidents au CNRS

[![alt text](/_/gdi/images/MindMap.png "Cartographie de la Gestion d'incidents au CNRS")](https://aresu.dsi.cnrs.fr/IMG/pdf/Incidents.pdf)


## Roue de Deming (PDCA)

<div style="text-align: center;">
    <img src="/_/gdi/images/PDCA.jpg" width="70%" />
</div>

* Planifier
* Développer
* Contrôler
* Ajuster