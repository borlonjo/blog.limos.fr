## Consolider <img src="/_/images/PDCA/PDCA-A.png" width="20%" align="right"/>

<div style="text-align: center;">
    <img src="/_/gdi/images/colmater.jpeg" width="70%"/>
</div>


## Consolider <img src="/_/images/PDCA/PDCA-A.png" width="20%" align="right"/>
### Objectifs</h3>
* Augmenter le niveau de sécurité
* Capitaliser l’expérience acquise
* Accompagner et responsabiliser l’ensemble des acteurs (hiérarchie, prestataires, utilisateurs, etc.)


## Consolider <img src="/_/images/PDCA/PDCA-A.png" width="20%" align="right"/>
### Préparer un bilan de l’incident</h3>
* Si besoin, effectuer des investigations complémentaires
  * se faire aider si besoin
    * Prestataire de confiance
    * Informaticien CNRS ou partenaire
    * Réseau SSI
    * Etc.
* Ecrire un rapport factuel et détaillé incluant le journal de l’incident


## Consolider <img src="/_/images/PDCA/PDCA-A.png" width="20%" align="right"/>
* Préparer un plan d'amélioration
  * Proposer des actions concrètes d'amélioration évaluées selon plusieurs critères (charge, coût, complexité, délai, ...) afin de permettre la prise de décision
  * Si nécessaire, proposer également des améliorations du processus de gestion des incidents de l'entité
* Compléter par une estimation du coût et des conséquences de la « non-sécurité »
  * Valoriser les impacts financiers liés à la perte ou la compromission des informations
  * Valoriser les coûts des actifs supports volés ou perdus
  * H.J. dédiés à la gestion de l’incident
  * H.J. perdus dû à l’incident
  * Perte de confiance ou d’image de marque


## Consolider <img src="/_/images/PDCA/PDCA-A.png" width="20%" align="right"/>

<div style="text-align: center;">
  <a href="https://extra.core-cloud.net/collaborations/RSSI-CNRS/tableau_de_bord/Incidents/R%C3%A9partition%20des%20incidents%20par%20cat%C3%A9gorie.aspx">
    <img src="/_/gdi/images/stat_cnrs_1.png"/>
  </a>
</div>


## Consolider <img src="/_/images/PDCA/PDCA-A.png" width="20%" align="right"/>

<div style="text-align: center;">
  <a href="https://extra.core-cloud.net/collaborations/RSSI-CNRS/tableau_de_bord/Incidents/Statistiques%20mensuelles%20des%20incidents.aspx">
    <img src="/_/gdi/images/stat_cnrs_2.png"/>
  </a>
</div>


## Consolider <img src="/_/images/PDCA/PDCA-A.png" width="20%" align="right"/>
## Clôturer l’incident
* Dans un délai raisonnable: réaliser un retour d’expérience (fortement recommandé)
  * Expliquer de façon factuelle en séance ce qu’il s’est passé sur la base du rapport
  * Inviter les différentes parties prenantes, dont la hiérarchie
  * Envoyer le rapport et la proposition de plan d’actions à l’avance
* Impliquer la hiérarchie
  * Si possible et si nécessaire, lors de la même séance, faire évoluer et dans tous les cas, faire valider le plan d'actions par les parties prenantes et la hiérarchie
* A la fin de cette séance, même s’il reste des actions, l’incident peut être considéré clôturé.


## Clôturer l’incident <img src="/_/images/PDCA/PDCA-A.png" width="20%" align="right"/>
### Actions post-incidents</h3>
* Consolider (Créer) la base d'incidents
* Consolider (Commencer) l‘analyse de risque
* Suivre le plan d'actions
  * Intégrer le suivi de réalisation de ces actions dans un comité existant (recommandé) ou créer un comité ad-hoc
  * Rendre compte à la hiérarchie de l’avancée des actions
* Partager la mésaventure et l’expérience avec les réseaux d’informaticiens
* S’il y a eu dépôt de plainte, répondre aux services de police et suivre l’avancée des procédures judiciaires