## Formalisation

<div style="text-align: center;">
    <img src="/_/gdi/images/GdI-incidents.png" width="70%"/>
</div>


## qu'est ce qu'un incident de sécurité?

* violation d'un des critères de sécurité de l'information
 * confidentialité
 * intégrité 
 * disponibilité


## [Gestion de risques](https://www.cases.lu/fr/gestion-du-risque.html)
### ici avec la terminologie [EBIOS](http://www.ssi.gouv.fr/IMG/pdf/EBIOS-1-GuideMethodologique-2010-01-25.pdf)
* utilise plusieurs éléments de la méthodologie
  * notion de scenarii
  * notion de vraissemblance / probabilité / occurrence
  * les échelles
  * les bases de connaissances
* doit être caractérisé par un type
  * vol ou perte
  * intrusion
  * ingénierie sociale (arnaque, phishing, etc.)
  * violation des règles (P2P illégal)
  * Déni de services
  * Vulnérabilité découverte mais a priori non exploitée


## Sources de Menaces 
### Caractéristaion
* origine humaine 
  * intentionnelle
    * motivations
  * accidentelle
* origine non humaine
  * naturelle
  * animale
  * contingence
* facilité d'accès au sujet de l'étude (interne ou externe)
* capacités (force intrinsèque, selon leurs ressources, expertise, dangerosité…)
  * notion pertinente en analyse de risques
  * inutile dans le cas de la gesiton d'incident (l'attaque a eu effectivement eu lieu)


## Sources de Menaces <!-- .element: id="menace" -->
### Classification
* Source humaine interne, malveillante
* Source humaine externe, malveillante
* Source humaine interne, sans intention de nuire
* Source humaine externe, sans intention de nuire
* Code malveillant d'origine inconnue
* Phénomène naturel
* Catastrophe naturelle ou sanitaire
* Activité animale
* Événement interne


## Motivations
* Jeu, défi
* Curiosité
* Argent, cupidité, gain financier
* Destruction, vandalisme, sabotage
* Vengeance
* Rébellion
* Idéologie, avantage politique
* Chantage
* Ego, amour-propre, couverture médiatique
* Recherche d'un avantage concurrentiel, espionnage économique, renseignement
* Terrorisme
* Erreurs et omissions involontaires


## Actifs
* Actifs primordiaux
  * Processus et activités métier
  * Informations
* Actifs support
  * Matériel
  * Logiciels
  * Réseau
  * Personnel
  * Site
  * Structure de l'organisation
* Menace
  * Cible les actifs primordiaux
  *Exploite les vulnérabilités des actifs en support


## Vulnérabilités<!-- .element: id="vulnerabilite" -->
* A identifier au mieux 
  * Déterminer les versions des produits
    * trouver des vulnérabilités connues
      * Si possible déterminer le <a href="http://cve.mitre.org/index.html">CVE (Common Vulnerabilities and Exposures)</a>


## Exploits
* Mode opératoire de l’attaquant
  * Récupérer le maximum d’informations sur
    * Actions effectuées        
    * Outils, programmes, scripts utilisés


## Impacts, conséquences
* Types dimpacts et conséquences retenus pour le CNRS
  * Pertes financières
  * Désorganisation interne
  * Atteinte à l’image de marque
  * Pertes du potentiel scientifique et technique
  * Condamnation et contentieux
  * Sécurité des personnes et  atteintes à l'environnement
* Evaluation
  * Immédiat : perte d'un critère de sécurité (DIC)
    * Pertes directes (vol ou perte)
    * Temps passé à la résolution de l'incident
    * Temps perdu suite à une indisponilibité ou à la restauration de l'intégrité
  * Plus ou moins long terme


## Gravité
* Evaluation une fois l'analyse de l'incident terminé
* Mêmes échelles que pour l’appréciation des risques
* Estimation des conséquences potentielles en fonction des éléments disponibles
  * Prévoir le pire
  * Doit être en permanence reconsidérée
* Les quasi incidents
  * Les dommages ont été évités par chance
  * Découverte de vulnérabilités
  * Doit être évalué comme un risque