## Contexte

<div style="text-align: center">

![alt text](/_/gdi/images/contexte.jpg "Contexte") <!-- .element: width="80%" -->

</div>


## Contexte
* Les états ont/vont renforcer la règlementation pour combattre les failles et les attaques [LPM/SAIV/OIV](http://www.defense.gouv.fr/actualites/dossiers/la-loi-de-programmation-militaire-lpm-2014-2019), [PPSIE](http://www.ssi.gouv.fr/fr/menu/actualites/le-premier-ministre-dote-l-etat-de-sa-premiere-politique-globale-de-securite.html), [Les différents plans gouvernementaux](http://www.ssi.gouv.fr/fr/defense-des-si/les-plans-gouvernementaux/), ...
* L’incident n’est plus l’exception mais la règle, il faut 
  * industrialiser son traitement
  * limiter l’impact direct sur l’information, les processus métiers, l’organisme, les personnes
  * limiter l’impact indirect sur les équipes en charge du traitement
  * se protéger, c’est aussi savoir réagir !

Note:
- Loi contre le terrorisme 2014
- Loi pour le renseignement 2015
- contexte tendu depuis les attentats
- piratage récent Sony / TV5 monde ... Thalès


## Obligation légale <!-- .element: id="legal" -->
* [Règlement européen du 24/06/2013](http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2013:173:0002:0008:FR:PDF")
* [PSSIE (Article 7) - 17 jullet 2014](http://www.ssi.gouv.fr/IMG/pdf/pssie_anssi.pdf)
  * *"Chaque entité contribue à la protection et à la défense des systèmes d'information de L'Etat par la mise en place d'une "chaîne opérationnelle", qui rend compte régulièrement à la chaîne fonctionnelle SSI"*
* [Objectifs de sécurité [ANSSI]- 27 férvier 2014](http://www.ssi.gouv.fr/IMG/pdf/20140310_Objectifs_de_cybersecurite_document_public.pdf)
  * 5/ Gérer les incidents de cybersécurité
* [Informatique et liberté (article 34 bis)](http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460#LEGIARTI000024504700)
  * Violation des [données à caractère personnel](http://www.cil.cnrs.fr/CIL/spip.php?rubrique299a)
    * déclaration à la CNIL et à l’intéressé
* [Arrêté du 3 juillet 2012 ](http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000026140136&dateTexte=) relatif à la PPST
    * Article 1 : signalement des incidents majeurs
* [Charte informatique du CNRS (3.2 Règles d’utilisation)](http://www.cil.cnrs.fr/CIL/IMG/pdf/charte_info.pdf)

Note:
- Niveau Francais, Défense, IL pour l'utilisateur, employeur, Européen
- d'où cette formation


## Objectifs
* Maintenir les compétences
* Mettre en place une "bonne pratique" supplémentaire
* Renforcer l’expertise interne existante
* Préparer une formation "investigation"
* Préparer le développement d’un réseau national collaboratif d’experts
* Anticiper le passage à un système de détection – réaction plus efficace


## de la [sécurité nationale](http://fr.wikipedia.org/wiki/S%C3%A9curit%C3%A9_nationale) à la SSI au CNRS           
### [La défense en profondeur](http://circulaire.legifrance.gouv.fr/pdf/2009/04/cir_2014.pdf)

* Sébastien Le Prestre de Vauban
            
<div style="text-align: center;">
    <img src="/_/images/fortification-a-la-vauban.jpg" width="50%" />
</div>
            
* exploiter plusieurs techniques de sécurité afin de réduire le risque lorsqu'un composant particulier de sécurité est compromis ou défaillant

Note:
- circulaire à lire
- un mot sur la défense nationale / défense globale / sécurité nationale


## Stratégie de la défense en profondeur
* n'évite pas l'attaque
  * ralentit l'attaquant en lui opposant des difficultés multiples et variées
    * **chacun** est un maillon de la sécurité nationale
      * **chacun** est donc responsable
        * de l'analyse des risques inhérents à son périmètre pour mieux les maîtriser
        * de l'anticipation et de la prévention des accidents et des actes de malveillance
        * **de l'amélioration continuelle de la sécurisation de son périmètre**
          * le risque 0 n'existe pas
          * la sécurité peut toujours être améliorée


## la chaîne Sécurité Défense

<div style="text-align: center;">
    <img src="/_/ssi/images/ssi/organisation_nationale_sdn.png" width="50%" />
</div>

Note:
- FSD CNRS Philippe Gasnot
- Le haut "socle commun avec les partenaires"
- le bas "niveau établissements"


## la chaîne SSI

<div style="text-align: center;">
    <img src="/_/ssi/images/ssi/organisation_nationale.png" width="50%" />
</div>

Note:
- AQSSI Alain Fuchs


## la chaîne sécurité au CNRS

<div style="text-align: center;">
    <img src="/_/gdi/images/organisation_securite_cnrs.png" />
</div>

Note:
- RSSI DR7 Gaetan Dardy
- RSSI CNRS M. Parache a démissionné et va être bientôt remplacé…


## La SSI niveau national CNRS
* Un RSSIC nommé par le président et placé, sous l’autorité du DGDR, à la DSI dispose d’une équipe    
  * Un RSSIC adjoint
  * Un RSSI DSI
  * Dont les missions principales sont  
    * L’animation du réseau SSI du CNRS
    * La définition et le suivi du plan d’action national SSI qui est présenté devant le Comité de Pilotage de la SSI du CNRS
    * La prise en compte des directives SGDSN – Ministère en lien avec les partenaires du domaine ESR
  * Qui est également chargé de mission auprès du Fonctionnaire de Sécurité et de Défense (FSD) du CNRS
    * pour garantir la conformité des actions SSI avec la protection du potentiel scientifique et  technique

Note:
- RSSIC adjoint Francois Morris
- RSSI DSI Jérémie Boutard


## La SSI niveau régional CNRS
* Un RSSI de DR
  * nommé, dans chaque délégation régionale après avis du RSSIC
  * anime le réseaux des chargés de sécurité des systèmes d’information (CSSI) des unités suivies par sa délégation
* La CRSSI
  * Le RSSI DR s’entoure, en accord avec le RSSIC, d’experts SSI appartenant à des unités de recherche, pour former une coordination régionale de la SSI (CRSSI)


## La SSI niveau unité CNRS
* Le DU est responsable de la SSI
* [Le Chargé de sécurité des systèmes d’information (CSSI)](https://aresu.dsi.cnrs.fr/spip.php?article120) 
  * nommé par le directeur d’unité, après avis du RSSI régional
  * assiste son DU dans l’exercice de sa responsabilité en matière de SSI
  * assiste son DU pour la mise en œuvre de la PSSI du CNRS dans son unité
  * sensibilise les agents à la SSI
  * met en œuvre les recommandations SSI transmises par le RSSI du CNRS
  * gère les alertes et incidents

Note:
- responsabilité du DU exemple du chiffrement


## Point de contact
* Toute unité doit en avoir un
  * Un événement lié à la sécurité de l'information doit être rapporté, enregistré, traité au niveau local
    * Si signalé à un niveau supérieur, sera systématiquement répercuté au niveau local
    * Au cas où le niveau local serait impliqué, possibilité de signalement au niveau supérieur
  * Publié, connu de tous
    * Noms des personnes
    * Mèl
    * Téléphone
  * Assistance utilisateurs si elle existe
  * Souvent CSSI, ASR
  * Peut être mutualisé entre plusieurs unités
  * Pas nécessairement un spécialiste de la sécurité


## ISIRT (Information Security Incident Response Team)
<div style="text-align: center;">
    <img src="/_/gdi/images/isirt_cnrs.png" />
</div>


## Remontée et veille en sécurité CNRS

<div style="text-align: center;">
    <img src="/_/gdi/images/remontee_veille_cnrs.png" />
</div>


## [Espace de travail collaboratif dédié à la SSI](https://extra.core-cloud.net/collaborations/RSSI-CNRS)

* Partagé
  * RSSI + Adjoints (partenaires compris)
  * CRSSI
  * CSSI + Adjoints
  * 1 liste d’ASR pour la diffusion des alertes hebdomadaires
* Prévention
  * Documentation sur la sensibilisation des utilisateurs
  * Enquête (vivante) sur le déploiement du chiffrement
* Politique de sécurité
  * PGSSI du CNRS dont la charte SSI du 29/11/2013
  * PSSI opérationnelle pour les laboratoires
  * Liste des règles classée par chapitre (sensibilité notée 1 à 3 *)
* Un système de déclaration des incidents utilisant un workflow

Note:
- le live CD d'A2IMP
- des fiches techniques liés aux événements d'actualités
- ces supports et ceux des autres DR