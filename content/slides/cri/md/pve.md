# Proxmox

## aka pve


## Proxmox

* 10 ans


## interfacer avec le LDAP

# https://forum.proxmox.com/threads/how-to-transfer-large-iso-to-proxmox.455/

iptables -t nat -I PREROUTING -d 37.59.57.58 -p tcp --dport 443 -j DNAT --to-destination 37.59.57.58:8006

apt install iptables-persistent

service iptables save

# https://pve.proxmox.com/wiki/HTTPS_Certificate_Configuration_(Version_4.x,_5.0_and_5.1)

# https://pve.proxmox.com/wiki/Storage:_Directory

vi /etc/pve/storage.cfg

dir: local
        path /var/lib/vz
        vgname pve
        content iso,vztmpl,backup,rootdir,images

service pvestatd restart


# https://pve.proxmox.com/wiki/Cloud-Init_FAQ

Hardware > Add > loudInit Drive

SCSI / 0 / local


# https://pve.proxmox.com/wiki/Cloud-Init_Support

# create local-lvm (https://pve.proxmox.com/wiki/Storage)


# download the image
wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img

# create a new VM
qm create 9001 --memory 2048 --net0 virtio,bridge=vmbr0

# import the downloaded disk to local-lvm storage
qm importdisk 9001 bionic-server-cloudimg-amd64.img local

# finally attach the new disk to the VM as scsi drive
qm set 9001 --scsihw virtio-scsi-pci --scsi0 local:vm-9001-disk-0

qm set 9000 --ide2 local:cloudinit
qm set 9000 --boot c --bootdisk scsi0
qm set 9000 --serial0 socket --vga serial0
qm template 9000

Deploying Cloud-Init Templates

qm clone 9000 123 --name isp
qm set 123 --sshkey /root/limosadm.pub
qm set 123 --ipconfig0 ip=10.0.10.123/24,gw=10.0.10.1
