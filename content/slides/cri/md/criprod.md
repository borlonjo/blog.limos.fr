# criprod
#### l'informatique qui marche!


## galactica

* navire sans capitaine
* plus d'image package
* plus de support sur les problèmes
* pas de mise à jour de la plateforme
* héberge des services critiques

* fonctionne à date
* ok pour la recherche
* source d'inspiration


## Et si ...

on réfléchissait au prochain système de virtualisation

* maintenable
* redéployable
* scalable
* utilisable

## par tous!

fini les SPOFs

#### on gère tous un datacenter home made!


## loi de Murphy

* tout cassera à un moment ou à un autre
  * mauvaise config
  * mise à jour foireuse
  * piratage
  * panne matérielle

au plus mauvais moment si possible


## loi de Murphy

* réparer c'est bien mais ce n'est pas toujours possible
  * temps d'investigation
  * données perdues
  * configuration manquantes
  * expertise


## Alors?

* On redéploie
  * vite
  * souvent
  * les yeux fermés
  * tout le datacenter de préférence
    * toutes les machines et tous les services


## komankonfé

il nous faut

* des routines de sauvegarde
* des routines de déploiement
* des routines de restauration


## en terme de ressources ...

* 2 (cluster d')hyperviseurs
  * un pour la prod
  * un pour la redondance

* un serveur pour
  * sauvegarder la prod
  * déployer et restaurer tous les services sur l'hyperviseur


# TOUTES LES NUITS!


## chaque service a un double

* prêt à prendre le relais
* tous les déploiements sont testés toutes les nuits

### On a plus peur!


## et t'as des machines pour ça

* récupérons tout
  * on veut que ça pète
    * pour se persuader qu'on sait remonter les yeux fermés


#### Pour faire de la bonne cuisine il faut ...

* des outils / ustensiles
  * un **cluster** d'hyperviseur **proxmox**
  * **terraform** pour crééer TOUTES les VMs nécessaires à nos services
    * **terraform** maintient l'état (en term d'inventaire) de notre **datacenter**


#### Pour faire de la bonne cuisine il faut ...

* des recettes
  * des déploiements automatisé avec **ansible** ou **maison**
    * versionnés sur un **gitlab** accessible en ldap ET avec le mot de passe root

* des ingrédients
  * un gestionnaire de secret **vault** accessible en ldap ET avec le mot de passe root


## PCA / PRA

* Donnez nous
  * un **gitlab** avec les bonnes recettes de déploiement
  * un fichier texte **terraform** décrivant l'état du datacenter
  * les mots de passe nécessaires au déploiement

## Et on reconstruit tout!
### en 1 ligne de commande


### Est ce qu'on fait pas chier pour rien?

* les routines de déploiements
  * sont agnostiques de l'hyperviseur
  * sont autodocumentées

* l'infra est reproductible
  * sur aws, google cloud, ...
  * sur openstack (mouahahaha!)

c'est un bonne opportunité de peaufiner le nettoyage entamer il y bientôt 2 ans


# les devs peuvent nous aider
## si on dit s'il te plaît


## On pourra mettre "DevOps" comme skills linkedin ...

## ... Et parler de sujet fun à l'apéro ;)


## Next step 1

* présentation de **proxmox**
  * son installation (william)
  * son utilisation avec **terraform**

* réflexion pour que tout le cri (voir l'équipe dev) puissent créer des vms
  * chacun pousse sa vms avec un playbook offert pour la maison
    * y a rien à casser


## Next step 2

* Présentation d'**ansible** au travers de 3 services: **gitlab**, **vault**, **my isima**

* réflexion pour que tout le cri ait en tête les bonnes pratiques pour écrire & architecturer correctement ses playbooks ansible ou maison
  * un projet a un cycle de vie probablement standard (install, configure, initialize, restore, backup)


## Next step 3

* Présentation de **vault**
  * consulter / écrire des secrets

* réflexion pour automatiser l'écriture des secrets dans la configuration de nos playbooks
  * les devs peuvent nous aider


## Next step 4

* Présentation de HaProxy et de la gestion des certificats **SSL**
  * créer une entrée dans **HaProxy**
  * créer une entrée dans **Bind**
  * rendre une machine visible sur un port
  * installer son certificat

* réflexion pour automatiser l'écriture de la configuration **HaProxy**, **DNS**, et **ssl**
  * les devs peuvent nous aider


## Next step 5

* mise en place du backup et du cluster backup avec redéploiement automatique quotidien
* chacun migre ses services
  * et se pose la question de son binôme pour ce service


## Next step 6

* c'est déjà la rentrée ;)


## Next step 7

* démontage de galactica au profit de cluster de proxmox pour la recherche ;)


## Next step 8

![apéro](images/apero.gif "apéro")<!-- .element: width="85%" -->
