# vault

![vault](images/vault.png "vault")<!-- .element width="30%" -->

**By HashiCorp**


## Installation

* téléchargement d'un binaire

  * [https://releases.hashicorp.com/vault/](https://releases.hashicorp.com/vault/)
  * décompresser dans /usr/local/bin
  * configurer les permissions
  * serveur
    * créer un service systemd
  * cli
    * `vault`


## Configuration

* /etc/vault/vault.hcl

```
backend "file" {
  path = "/var/lib/vault"
}
ui = true
disable_mlock = true
listener "tcp" {
  address     = "10.0.0.1:8200"
  tls_disable = 1
}
```


## [<i class="fa fa-book" aria-hidden="true"></i> Secret engine](https://www.vaultproject.io/docs/secrets/)

* [<i class="fa fa-book" aria-hidden="true"></i> Secrets Engines - getting started](https://learn.hashicorp.com/vault/getting-started/dynamic-secrets)
* [<i class="fa fa-book" aria-hidden="true"></i> AWS Secrets Engine](https://www.vaultproject.io/docs/secrets/aws/index.html)
* [<i class="fa fa-book" aria-hidden="true"></i> Active Directory Secrets Engine](https://www.vaultproject.io/docs/secrets/aws/index.html)
* [<i class="fa fa-book" aria-hidden="true"></i> SSH Secrets Engine](https://www.vaultproject.io/docs/secrets/ssh/index.html)
* [<i class="fa fa-book" aria-hidden="true"></i> KV Secrets Engine](https://www.vaultproject.io/docs/secrets/kv/index.html)


## KV

```shell
$ vault kv get secret/test
====== Data ======
Key          Value
---          -----
password1    secret$

$ vault kv put secret/test password2=secret!
Success! Data written to: secret/test

$ vault kv get secret/test
====== Data ======
Key          Value
---          -----
password2    secret!
```


## KV2

```shell
vault login token=<root-token>
vault secrets enable -path=cri kv
vault kv enable-versioning secret/ # kv2
```

* les secrets sont versionés
* il est possible d'utiliser PATCH et pas seulement PUT

```shell
$ vault kv patch secret/test password1=secret$
Success! Data written to: secret/test

$ vault kv get secret/test
====== Data ======
Key          Value
---          -----
password1    secret$
password2    secret!
```


## Authentification

* par token
  * root
  * d'application

* par ldap
  * en réalité génère un token dans ~/.vault-token contenant


## LDAP

```shell
$ vault write auth/ldap/config \
    url="ldaps://samantha.local.isima.fr" \
    userattr="sAMAccountName" \
    userdn="dc=local,dc=isima,dc=fr" \
    groupattr="cn" \
    groupfilter="(&(objectClass=group)(member:1.2.840.113556.1.4.1941:={{.UserDN}}))" \
    groupdn="ou=GROUPES_LOCAUX,dc=local,dc=isima,dc=fr" \
    binddn="cn=vault,ou=Comptes de Services,dc=local,dc=isima,dc=fr" \
    bindpass="secret" \
    insecure_tls="false" \
    starttls="true"
```

[<i class="fa fa-book" aria-hidden="true"></i> LDAP Auth Method](https://www.vaultproject.io/docs/auth/ldap.html)


## Policy

/etc/vault/cri.hcl

```
# Write and manage secrets in key-value secret engine
path "secret/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# To enable secret engines
path "sys/mounts/*" {
  capabilities = [ "create", "read", "update", "delete" ]
}

path "cubbyhole/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

```

```shell
$ vault policy write cri /etc/vault/cri.hcl
```


## appliquer une policy à un groupe ldap

```shell
$ vault write auth/ldap/groups/cri policies=cri
```


## Utilisation

* [binaire à télécharger](https://releases.hashicorp.com/vault/)
  * cross plateform
  * deux variables d'environnement
    * $VAULT_ADDR=https://vault.isima.fr
    * $VAULT_TOKEN ou authentification ldap
ou
* l'[<i class="fa fa-book" aria-hidden="true"></i> api](https://www.vaultproject.io/api/overview)


## Workflow

```shell
$ vault login -method=ldap username=vimazeno
$ vault secrets list
$ vault list secret/
$ vault kv get  secret/tokens
$ vault kv get  secret/tokens # à chaque put on écrase les entrées qu'on ne réécrit pas
$ vault kv get  secret/tokens -format=json
$ vault kv get  secret/tokens -format=json | jq .data
$ vault kv get secret/tokens -format=json | jq .data.password
$ vault kv put secret/tokens password2=$(date | sha256sum | cut -c -50)
$ vault kv patch secret/tokens password1=$(date | sha256sum | cut -c -50)
$ vault delete secret/tokens
```


## création de token

my.hcl

```
path "secret/data/cri/apps/my" {
  capabilities = ["create", "read", "update", "delete", "list"]
}
```

```shell
$ vault policy write vault/apps/my.hcl
$ vault token create -policy=my
```


## vault/ci/cd

### en local

authentification ldap

### <i class="fa fa-gitlab" aria-hidden="true"></i> CI / CD

![vault CI](images/vault-ci.png)


## bin/setup

<small>
```bash
command -v "vault" >/dev/null 2>&1 || {
  echo >&2 "I require vault to run see stack"
  exit 1
}
if [[ -z "${VAULT_ADDR}" ]] ; then
  export VAULT_ADDR=https://vault.isima.fr
fi
if [[ -z "${VAULT_TOKEN}" ]] ; then
  if [[ -z "${VAULT_USERNAME}" ]] ; then
    echo uca username
    read username
    export VAULT_USERNAME=${username}
  fi
  vault login -method=ldap username=$VAULT_USERNAME > /dev/null
  echo " export VAULT_TOKEN=$(cat ~/.vault-token)"
else
  vault login token=${VAULT_TOKEN} > /dev/null
fi
```
</small>


## bin/configure

<small>
```bash
# lecture des clés vault avec python: la sortie est une liste python UTF8 (u'value')
KV=$(vault read cri/my -format=json | python -c "import sys, json; print json.load(sys.stdin)['data'].keys()")
# converison de la liste python en liste bash
VAULT_KEYS=( $(echo ${KV} | sed -r "s/', u'/' '/g" | sed -r "s/\[u'/'/g" | sed -r "s/\]//g") )
# copie du ttemplatye de configuration en fichier de configuration
cp config.sample.py config.py
# itération sur les clés vault
for i in "${VAULT_KEYS[@]}"
do
  # enlève le permier '
  i=${i%\'}
  # enlève le dernier '
  i=${i#\'}
  sed -i "s|$i|$(vault read cri/my -format=json | jq -r .data.$i | sed -r "s/\n//g")|g" config.py 2>/dev/null
done
```
</small>
