# DevOps

![DevOps](images/devops-toolchain.png "DevOps")


## Pourquoi?

* unifier
   * développement logiciel (dev)
   * administration des infrastructures (ops)
* automatiser (automation)
* suivre (monitoring)
* tester: automatiquement
* accélérer: cycles courts à prioriser
* déployer: automatiquement & souvent!


## contexte ISIMA / LIMOS

![team cri](images/cri.jpeg "team cri")

* ambiance tendue entre dev et ops !
* on cherche à
  * automatiser la gestion de nos services
  * autonomiser les dev et idéalement les users


## De nombreux outils

![DevOps tools](images/devops-tools.png "DevOps tools")


## les outils du dialogue

* [vagrant](vagrant.html)
* [ansible](ansible.html)
* [vault](vault.html)
* [proxmox](pve.html)
* [terraform](terraform.html)
* [gitlab](gitlab.html)


## bws aka la stack

!["stack"](images/stack.png "stack")


## See also

* [<i class="fa fa-wikipedia"></i> DevOps](https://en.wikipedia.org/wiki/DevOps)

* [12 factor](https://12factor.net/)

* [@waxzce](https://twitter.com/waxzce)
