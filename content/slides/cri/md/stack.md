# stack

![stack](images/aws.png "aws")<!-- .element width="30%" -->

aka **B** rew **W** ired **S** tack


## requirement

* proxmox aka pve
* ansible
* terraform
  * [proxmox-provisionner](https://github.com/Telmate/terraform-provider-proxmox)
  * [proxmox-api](https://github.com/Telmate/proxmox-api-go)
*  du love <3


## soyons honnête

Tous repose sur

* [proxmox-provisionner - pulse](https://github.com/Telmate/terraform-provider-proxmox/pulse)
* [proxmox-api - pulse](https://github.com/Telmate/proxmox-api-go/pulse)


## PCA / PRA
