Title: Profan
Date: 2017-01-06 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, mining


<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
        <span class="sr-only">70% complete</span>
    </div>
</div>

![ProFan](images/projets/ProFAN.jpg)

se donne pour ambition de promouvoir et de qualifier, par la nature de leurs effets, de nouveaux contextes d'apprentissage et d'enseignement afin de favoriser l'acquisition de compétences nouvelles pour répondre aux exigences des métiers du futur. Ce projet fait parti du Programme d'investissements d'avenir du ministère de l'éducation nationale. Cette expérimentation est centrée sur une plateforme numérique qui permettra l'organisation et la bonne tenue de l'expérimentation ainsi que la collecte de données exploitables par la recherche en sciences cognitives dans 79 lycées professionnels.

Plus d'informations http://bit.ly/2ruwkBx

Pour ce projet

  * collecte les besoins et défini d'une architecture technique
  * participation au recrutement d’un développeur pour la conception de la plateforme
  * encadrement d'un développeur, d'un stagiaire et d'un autre développeur à temps partiel
  * coordination avec
      * les rectorats, le service des sytèmes d'information (SI) et le service d'analyse statistiques de l'éducation nationale pour la récupération des données concernant les lycéens, l'authentification des lycéens et professeurs et le déploiement de l'application
      * les chercheurs en sciences sociales et cognitives pour la mise en place d'une dizaine de questionnaires et d'une demi douzaine outils d’évaluation
